import * as d3 from "d3";
import { sankey, sankeyLinkHorizontal, sankeyCenter } from "d3-sankey";
import React from 'react';
import Select from './custom.select';
//import Budget1B from './Budget1B';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function BudgetSup(){
  let w = window.innerWidth,
      h = 400;
  let margin = {top: 30, left:20, bottom: 25, right:5};
  let budgetData;
  let currVars = {state: 0, year: 0};
  let vars = {states: ["ce", "pr", "sp"],
              dept:["TRIBUNAL DE JUSTIÇA","DEFENSORIA PÚBLICA","MINISTÉRIO PÚBLICO"],
              years: [2018,2017,2016,2015,2014,2013,2012] };
  let selectors;
  let chartH, chartW;
  let colors = {
    "TRIBUNAL DE JUSTIÇA": "#F7D841",
    "DEFENSORIA PÚBLICA": "#5AB755",//"#5AB755"
    "MINISTÉRIO PÚBLICO": "#D33145"//"#D33145"
  };
  let tooltip;

  function setup(){
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    let customSelects = document.querySelector("#budget2").querySelectorAll(".select");

    selectors = [];
    customSelects.forEach(function(e,i){
      selectors.push( new Select({target: e, callback: changeSelections}) );
    });

    // load national gender data
    d3.tsv("data/orcamento-sup-orgao.tsv", function(d,di,cols){
      for (var i = 0; i < cols.length; ++i){
        if(i > 3){ d[cols[i]] = +d[cols[i]]; }
      }
      return d;
    })
    .then(function(data) {
      budgetData = d3.nest()
      .key(function(d,i){
        return d.uf.toLowerCase();
      })
      .entries(data)

      draw();
    });
  }

  function draw(container, redraw){
    for(var i=0; i<vars.dept.length; i++){
      let svg = d3.select("#svg-budget-"+(2+i));
      buildVis(svg, vars.dept[i])
    }
  }

  function buildVis(svgTarget, dept){
    let container = d3.select(svgTarget.node().parentNode);

    container.select("h4").html(dept);

    w = container.style("width").slice(0, -2);
    h = 400;

    chartH = h - margin.top - margin.bottom;
    chartW = w - margin.left - margin.right;

    svgTarget.attr("viewBox", "0 0 "+ w +" "+h);
    svgTarget.selectAll("g").remove();

    let svg = svgTarget.append("g");
    svg.attr("transform", "translate("+margin.top+","+margin.left+")")


    let ufData = budgetData.filter(function(d,i){
      return d.key == vars.states[currVars.state];
    })[0].values;

    let deptData = ufData.filter(function(d,i){
      return d.orgao == dept;
    });

    let stackedData = d3.nest()
    .key(function(d){
      return d.orgao + " - " + d.ano;
    })
    .rollup(function(v){
      let sum;
      sum = d3.sum(v, function(g) {
        return +g.valor_sup;
      }).toFixed(2);
      return sum;
    })
    .entries(deptData);

    let maxY = d3.max(stackedData, function(d){
      return +d.value;
    });
    let minY = d3.min(stackedData, function(d){
      return +d.value;
    });
    if(minY>0){ minY = 0; }

    let x0 = d3.scaleBand()
    .domain(deptData.map(d => d.ano))
    .rangeRound([margin.left, chartW - margin.right])
    .paddingInner(0.2)

    if(w < 600){ x0.paddingInner(0.05); }

    let y = d3.scaleLinear()
    .domain([minY, maxY]).nice()
    .rangeRound([chartH-margin.bottom, margin.top]);

    let xAxis = g => g
    .attr("transform", `translate(0,${chartH - margin.bottom})`)
    .call(d3.axisBottom(x0).tickSizeOuter(0))
    //.call(g => g.select(".domain").remove())

    let yAxis = g => g
    .attr("class", "axis-y")
    .attr("transform", "translate(0,0)")
    .call(d3.axisLeft(y).tickFormat(function(d){
      let scale = (d/1000000);
      if(maxY > 1000000000){
        scale = (d/1000000000).toFixed(1)
      }
      return scale;
    }))//.ticks(null, "s")
    .call(g => g.select(".domain").remove())
    .call(g => g.append("text")
    .attr("y", 8)
    .attr("x", -25)
    .attr("fill", "#fff")
    .attr("text-anchor", "start")
    .attr("font-weight", "bold")
    .text(function(d){
      let t = "R$ milhões";
      if(maxY > 1000000000){
        t = "R$ bilhões";
      }
      return t;
    }))

    let yearGroup = svg.selectAll("g")
      .data(d3.nest().key(d => d.ano+" - "+d.orgao).entries(deptData))
      .enter()
      .append("g")
      .attr("id",function(d){
        let str = "bdg2-g-";
        let orgCap = d.key.split(" - ");

        str += orgCap[0] + "-" + orgCap[1].substr(0,1).toLowerCase();
        return str;
      })
      .attr("transform", function(d){
        let xPos = x0(+d.values[0].ano);
        return "translate("+xPos+",0)";
      })

    let stackedGroup = yearGroup.selectAll("g")
      .data(function(d){
        let obj = [];
        let ySum = 0;
        let item;

        d.values.sort(function(a,b){
          return Number(b.valor_sup) - Number(a.valor_sup);
        })

        for(var i=0; i<d.values.length; i++){
          d.values[i].yv = ySum;
          ySum += Number(d.values[i].valor_sup);
        }

        return d.values;
      })
      .enter()
      .append("g")
      .attr("transform", function(d){
        return "translate(5,0)";
      });

    stackedGroup.append("rect")
      .attr("rx", 4).attr("ry", 4)
      .attr("x",function(d){
        return 0;
      })
      .attr("y",function(d){
        let valH = d.valor_sup;
        let finalY = y(d.yv + Math.abs(valH));
        if(valH < 0){
          finalY = y(d.yv);
        }
        return finalY;
      })
      .attr("fill",function(d){
        let valH = d.valor_sup;
        let fill = colors[d.orgao];
        if(valH < 0){
          fill = "url(#diagBar2)"//patterns[d.orgao];//y(d.yv);
        }
        return fill;
      })
      .attr("fill-opacity",function(d){
        let valH = d.valor_sup;
        let op = 1;
        if(valH < 0){ op = 0.7; }

        return op;
      })
      .attr("stroke", "#000")
      .attr("stroke-opacity",function(d){
        return 0.5;
      })
      .attr("width",function(d){
        return (x0.bandwidth()-(w/40));
      })
      .attr("height",function(d){
        let valH = Math.abs(d.valor_sup);
        return y(0) - y(valH);
      })
      .on("mouseover", function(d) {
        let title = d.tipo;// + d.data.subfuncao + "</b><br/>(FUNÇÃO: "+d.data.funcao + ")";
        let titleStr = "<b>"+title+"</b>";
        let val, valStr = "";
        let milStr = " milhões"

        tooltip.classed("active", true);

        if(d.valor_sup){
          val = d.valor_sup;//d.data[vars.values[currVars.value]];

          if(Math.abs(val) > 1000000000){
            val = (val/1000000000).toFixed(1);
            milStr = " bilhões"
          }else if(Math.abs(val) < 100000){
            val = (val/1000).toFixed(1);
            milStr = " mil"
          }else{
            val = (val/1000000).toFixed(1);
          }

          // string de milhar singular
          milStr = Math.abs(val) > 1 ? " milhões": " milhão";
          valStr += "R$ " + val + milStr +"<br/>";
        }

        tooltip.html(titleStr + "<br/>" + valStr + "");

        positionTooltip();
      })
      .on("mousemove", function(d) {
        positionTooltip();
      })
      .on("mouseout", function(){
        tooltip.classed("active", false)
      });

    svg.append("g")
        .call(xAxis);

    svg.append("g")
        .call(yAxis);

    svg.select(".axis-y").selectAll(".tick")
    .attr("opacity",function(d,i){
      let display = 1;
      let isOdd = (i/2).toString().indexOf(".") == -1;
      if(h < 300 && !isOdd){
        display = 0;
      }
      return display;
    })
  }

  function positionTooltip(){
    let tW = d3.select("#tooltip").style("width").slice(0,-2);
    let tH = d3.select("#tooltip").style("height").slice(0,-2);
    let offset = {  x: tW/2,
                    y: tH/2}
    let tWidth = d3.select("#tooltip").style("width").slice(0,-2);
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};

    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y - 30;

    if(pos.x < 0){
      pos.x = 0;
    }else if(pos.x > window.innerWidth - tW){
      pos.x = window.innerWidth - tW;
    }

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function changeSelections(){
    selectors.forEach(function(s,i){
      let sel = s.node;
      let selStr = sel.options[sel.selectedIndex].value;
      let list = sel.parentNode.getAttribute("id").split("-")[2];
      let index = vars[list].indexOf(selStr.toLowerCase());

      currVars[list.substr(0,list.length-1)] = index;
    });

    draw();
  }

  function rightRoundedRect(x, y, width, height, radius) {
    return "M" + x + "," + y
       + "v" + (-radius - height)
       + "a" + radius + "," + (-radius) + " 1 0 1 " + radius + "," + (-radius)
       + "h" + (width - radius)
       + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
       + "v" + (height + radius)
       + "z";
  }
  function rectPath(x, y, width, height) {
    return "M" + x + "," + y
       + "v" + (-height)
       + "h" + (width)
       + "v" + (height)
       + "z";
  }

  window.addEventListener("resize", function(){

  });
  let initTimeout = setTimeout(setup, 200);

  return (
    <section id="budget2">
    <div id="budget2-container" className="chart-block">
      <h3>Suplementação orçamentária
      <div id="budget1-selects" className="selector-group">
        <div id="bdg-select-states" className="select">
          <select>
            <option value="CE">CE</option>
            <option value="PR">PR</option>
            <option value="SP" defaultValue>SP</option>
          </select>
        </div>
      </div>
      </h3>

      <div id="budget2-intro" className="intro-block">
        <p><b>Por Instituição do sistema de justiça</b></p>
      </div>
    </div>
    <div id="budget2-svg-container" className="chart-block-wide">
      <div className="third-block">
        <h4></h4>
        <svg id="svg-budget-2" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
        <defs>
          <pattern id="diagBar2" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke="#666" fillOpacity="0.3" strokeWidth="1.5" />
            <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#000" fillOpacity="0.5" strokeWidth="1.5" />
          </pattern>
        </defs>
        </svg>
      </div>
      <div className="third-block">
        <h4></h4>
        <svg id="svg-budget-3" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
        <defs>
          <pattern id="diagBar2" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke="#666" fillOpacity="0.3" strokeWidth="1.5" />
            <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#000" fillOpacity="0.5" strokeWidth="1.5" />
          </pattern>
        </defs>
        </svg>
      </div>
      <div className="third-block">
        <h4></h4>
        <svg id="svg-budget-4" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
        <defs>
          <pattern id="diagBar3" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke="#666" fillOpacity="0.3" strokeWidth="1.5" />
            <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#000" fillOpacity="0.5" strokeWidth="1.5" />
          </pattern>
        </defs>
        </svg>
      </div>
    </div>
    <div className="chart-block">
      <p className="chart-info">(Valores nominais, como informados pelo Poder Executivo Estadual)</p>
      <p className="chart-source"><b>Fonte:</b> Ceará - Estado do CE / Paraná - Estado do PR / São Paulo - Estado de SP
      &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b> JUSTA
      </p>
    </div>

    </section>
  )
}

export default BudgetSup;
