import * as d3 from "d3";
import React from 'react'
import Select from './custom.select';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function Egress(){
  let w = window.innerWidth,
      h = window.innerHeight*0.4;
  let svgW, svgH;
  let svg, svg2, x1, x2, y1, y2, y3, gX1, gY1, gX2, gY2;
  let focus, context;
  let zoom, brush, xAxis1, xAxis2, yAxis2, yAxis1, yAxisZoom;
  let margin = {top: 20, left:40, bottom: 40, right:0};
  let egData;
  let maxH = (h/6)*3;
  let states = ["ce", "pr", "sp"];
  let currState = 0;
  let selectors;
  let idleTimeout;
  let tooltip;

  function setup(){
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip");
    }
    let customSelects = document.querySelector("#egress").querySelectorAll(".select");

    selectors = [];
    customSelects.forEach(function(e,i){
      selectors.push( new Select({target: e, callback: changeSelections}) );
    });

    svg = d3.select("#svg-egress");
    svg2 = d3.select("#svg-egress-2");

    // load national gender data
    d3.tsv("data/egress1.tsv", function(d,di,cols){
      for (var i = 0; i < cols.length; ++i){
        if(cols[i]!="uf"){
          d[cols[i]] = +d[cols[i]];
        }
      }
      return d;
    })
    .then(function(data) {
        egData = {};

        for(var i = 0; i<states.length; i++){

          egData[states[i]] = data.filter(function(d,di){
            return d.uf.toLowerCase() == states[i].toLowerCase();
          });
        }

        draw();
    })
    /*.catch(function(error){
       console.log("error on 'egress'");
    });*/
  }

  function draw(){
    w = Number(svg.style("width").slice(0, -2));
    h = w*0.6;//svg.style("height").slice(0, -2);
    if(window.innerHeight > window.innerWidth){
      h = window.innerHeight*0.4;
    }else if(h < 400){
      h = 400;
    }
    svg.attr("viewBox", "0 0 "+ w +" "+h);
    svg2.attr("viewBox", "0 0 "+ w +" "+h);

    //console.log(d3.select("#eg-vis1"));
    svg.select("#eg-vis1").remove();
    svg2.select("#eg-vis2").remove();

    let spacer = 80;
    if(window.innerWidth < 480){ spacer = 50}

    svgH = h - margin.top - margin.bottom;
    svgW = w - margin.left - margin.right;

    zoom = d3.zoom()
    .scaleExtent([1, Infinity])
    .translateExtent([[0, svgH], [svgW, 0]])
    .extent([[0, 0], [20, svgH]])
    .on("zoom", onZoom);

    let group1 = svg.append("g").attr("id","eg-vis1")
      .attr("transform", "translate("+margin.left+","+margin.top+")");

    let group2 = svg2.append("g").attr("id","eg-vis2")
      .attr("transform", "translate("+margin.left+","+margin.top+")");

    /*
    brush = d3.brushY()
    .extent([[0, 0], [20, svgH]])
    .on("brush end", onBrushed);
    */

    // scales
    //y = d3.scaleLinear().range([svgH,0]);
    y1 = d3.scaleLinear().range([svgH,0]);
    y2 = d3.scaleLinear().range([svgH,0]);
    y3 = d3.scaleLinear().range([svgH,0]);
    x1 = d3.scaleBand().range([0,svgW]).padding(0.15);
    x2 = d3.scaleBand().range([0,svgW]).padding(0.15);
    //yZoom = d3.scaleLinear().rangeRound([svgH,0]);

    // create axis objects
    xAxis1 = d3.axisBottom(x1);
    xAxis2 = d3.axisBottom(x2);
    //xAxis2 = d3.axisBottom(y2).ticks(4);
    yAxis1 = d3.axisLeft(y2).ticks(6);
    yAxis2 = d3.axisLeft(y3).ticks(4);
    yAxis1.tickFormat(function(d){return (d/1000) + " mil"});
    yAxis2.tickFormat(function(d){return (Math.round(d/100000)/10).toFixed(1)});

    let max1 = d3.max(egData[states[currState]], function(d){
      let v = d.valor_loa;
      return v;
    })
    let max2 = d3.max(egData[states[currState]], function(d){
      let v = d.valor_liq;
      return v;
    });
    let max3 = d3.max(egData[states[currState]], function(d){
      let v = d.egressos;
      return v;
    });
    let max4 = d3.max(egData[states[currState]], function(d){
      let v = d.presos;
      return v;
    });
    let maxVal = d3.max([max1, max2]);

    y1.domain([0,d3.max([max1, max2])]);
    x1.domain(egData[states[currState]].map(function(d) { return d.ano; }));
    x2.domain(egData[states[currState]].map(function(d) { return d.ano; }));
    y2.domain([0,d3.max([max3, max4])]).nice(6);
    y3.domain([0,d3.max([max1, max2])]).nice(4);

    gX1 = group1.append("g")
      .attr("class", "axis axis-x")
      .attr("transform", "translate(0," + svgH + ")")
      .call(xAxis1);

    gX2 = group2.append("g")
      .attr("class", "axis axis-x")
      .attr("transform", "translate(0," + svgH + ")")
      .call(xAxis2);

    gY1 = group1.append("g")
      .attr("class", "axis axis-y")
      .attr("transform", "translate(5,0)")
      .call(yAxis1);

    gY2 = group2.append("g")
      .attr("class", "axis axis-y")
      .attr("transform", "translate(-10,0)")
      .call(yAxis2);

    group1.append("text")
      .attr("class", "axis-title")
      .text("Número de pessoas")
      .attr("text-anchor","start")
      .attr("x", -32)
      .attr("y", -10)

    group2.append("text")
      .attr("class", "axis-title")
      .text("R$ milhões")
      .attr("text-anchor","start")
      .attr("x", -32)
      .attr("y", -10)

    group1.selectAll(".eg-barD")
      .data(egData[states[currState]])
      .enter().append("rect")
        .attr("class", "eg-barD")
        .attr("fill","#F7D841")
        .attr("rx",4).attr("ry",4)
        .attr("y", function(d) {
          return y2(d.egressos);
        })
        .attr("x", function(d) { return x1(d.ano)})
        .attr("width", (x1.bandwidth()-(w/50))/2 - 1)
        .attr("height", function(d) {
          return svgH - y2(d.egressos);
        })
        .on("mouseover", function(d){ showBarInfo(d3.select(this), d, "egressos"); })
        .on("mousemove", function(d){ moveTooltip(d); })
        .on("mouseout", function(){ tooltip.classed("active", false) });

    group1.selectAll(".eg-barC")
      .data(egData[states[currState]])
      .enter().append("rect")
        .attr("class", function(d) {
          let projClass = "";
          if(d.projecao == 1){ projClass = " eg-proj" }
          return "eg-barC" + projClass;
        })
        .attr("fill","url(#diagBar)")
        .attr("rx",4).attr("ry",4)
        .attr("y", function(d) {
          return y2(d.presos);
        })
        .attr("x", function(d) {
          return x1(d.ano) + ((x1.bandwidth()-(w/50))/2) + 2
        })
        .attr("width", (x1.bandwidth()-(w/50))/2 - 1)
        .attr("height", function(d) {
          return svgH - y2(d.presos);
        })
        .on("mouseover", function(d){ showBarInfo(d3.select(this), d, "presos"); })
        .on("mousemove", function(d){ moveTooltip(d); })
        .on("mouseout", function(){ tooltip.classed("active", false) });

    let egDataAlt = egData[states[currState]];

    if(states[currState] == "sp"){
      egDataAlt = egDataAlt.filter(function(d,i){
        return (d.ano != "2013");
      })
      x2.domain(egDataAlt.map(function(d) { return d.ano; }));

      gX2.call(xAxis2.scale(x2))
    }
    group2.selectAll(".eg-bar2")
      .data(egDataAlt)
      .enter().append("rect")
        .attr("class", "eg-bar2")
        .attr("fill","#F7D841")
        .attr("rx",4).attr("ry",4)
        .attr("y", function(d) {
          return y3(d.valor_liq);
        })
        .attr("x", function(d) {
          return x2(d.ano);
        })
        .attr("width", x2.bandwidth()-(w/60))
        .attr("height", function(d) {
          return svgH - y3(d.valor_liq);
        })
        .on("mouseover", function(d){ showBarInfo(d3.select(this), d, "valor_liq"); })
        .on("mousemove", function(d){ moveTooltip(d); })
        .on("mouseout", function(){ tooltip.classed("active", false) });

    group2.selectAll(".eg-bar1")
      .data(egDataAlt)
      .enter().append("rect")
        .attr("class", "eg-bar1")
        .attr("fill","url(#diagBar)")
        .attr("fill-opacity",.8)
        .attr("rx",4).attr("ry",4)
        .attr("y", function(d) {
          return y3(d.valor_loa);
        })
        .attr("x", function(d) {
          return x2(d.ano) + (x2.bandwidth()-(w/60))*.15
        })
        .attr("width", (x2.bandwidth()-(w/60))*.7)
        .attr("height", function(d) {
          return svgH - y3(d.valor_loa);
        })
        .on("mouseover", function(d){ showBarInfo(d3.select(this), d, "valor_loa"); })
        .on("mousemove", function(d){ moveTooltip(d); })
        .on("mouseout", function(){ tooltip.classed("active", false) });


        if(states[currState] != "sp"){
          d3.select('#chart-info-sp').style('display', 'none');
        }else{
          d3.select('#chart-info-sp').style('display', 'block');
        }
        if(states[currState] != "ce"){
          d3.select('#chart-info-ce').style('display', 'none');
        }else{
          d3.select('#chart-info-ce').style('display', 'block');
        }
  }

  function onZoom(){
    // create new scale ojects based on event
    var newScaleX = d3.event.transform.rescaleX(x1)
    var newScaleY = d3.event.transform.rescaleY(y1)

    // update axes
    gX.call(xAxis.scale(newScaleX));
    gY.call(yAxis.scale(newScaleY));

    // update circle
    circles.attr("transform", d3.event.transform)
  };

  function showBarInfo(elem, d, prop){
    let val, valStr = "";
    let milStr = " mil";
    let type = elem.attr("class").split("bar")[1];

    val = d[prop];
    if(type == 1 || type == 2){
      valStr = "R$" + val.toLocaleString('pt-BR', {
        maximumFractionDigits: 2,
        minimumFractionDigits: 2,
        currency: 'BRL'
      });
    }else{
      valStr = val.toLocaleString('pt-BR') + " pessoas";
    }


    tooltip.classed("active", true);
    tooltip.html( valStr);

    let offset = {  x: d3.select("#tooltip").style("width").slice(0,-2)/2,
                    y: d3.select("#tooltip").style("height").slice(0,-2)/2}
    let tWidth = d3.select("#tooltip").style("width").slice(0,-2);

    moveTooltip(d);
  }

  function moveTooltip(d){
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};
    let offset = {  x: d3.select("#tooltip").style("width").slice(0,-2)/2,
                    y: d3.select("#tooltip").style("height").slice(0,-2)/2};

    let tWidth = d3.select("#tooltip").style("width").slice(0,-2);
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y - 30;

    if(pos.x < 0){
      pos.x = 0;
    }else if(pos.x > window.innerWidth - 200){
      pos.x = window.innerWidth - 200;
    }

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function getSmartTicks(val){
    var step = Math.pow(10, val.toString().length - 1);

    if (val / step < 2) {
        step = step / 5;
    } else if (val / step < 5) {
        step = step / 2;
    }

    var slicesCount = Math.ceil((val) / step);

    return {
      endpoint: slicesCount * step,
      count: Math.min(6, slicesCount) //show max 6 ticks
    };
  }

  function changeSelections(target){
    let sel = selectors[0].node;
    let selStr = sel.options[sel.selectedIndex].value;
    let list = sel.parentNode.getAttribute("id").split("-")[2];
    let index = states.indexOf(selStr.toLowerCase());

    currState = index;

    if(states[currState] == "ce"){
      d3.select("#eg-leg2").select(".movable")
      .style("display","none");
      d3.select('#chart-info-ce').style('display', 'block');
    }else{
      d3.select("#eg-leg2").select(".movable")
      .style("display","inline-block");
      d3.select('#chart-info-ce').style('display', 'none');
    }

    if(states[currState] == "pr"){
      d3.selectAll(".pr-note").classed("active",true);
      d3.select("#eg-topic2").style("display", "none");
      //d3.select("#eg-msg").classed("active", true);
    }else{
      d3.selectAll(".pr-note").classed("active",false);
      d3.select("#eg-topic2").style("display", "block");
      //d3.select("#eg-msg").classed("active", false);
    }

    if(states[currState] == "sp"){
        d3.select('#chart-info-sp').style('display', 'block');
    }else{
        d3.select('#chart-info-sp').style('display', 'none');
    }

    draw();
  }

  window.addEventListener("resize", function(){
    draw();
  });
  let initTimeout = setTimeout(setup, 200);

  return (
    <section id="egress">
    <div className="chart-block">
      <h3>
          Egressos do Sistema Prisional
        <div id="egress-selects" className="selector-group">
            <div id="egress-select-states" className="select">
            <select>
                <option value="CE">CE</option>
                <option value="PR">PR</option>
                <option value="SP" defaultValue>SP</option>
            </select>
            </div>
        </div>
      </h3>

      <div id="egress-intro" className="intro-block">
        <div className="eg text-block active">
          <p className="active">Número de presos e egressos no estado, por ano.</p>
        </div>
      </div>
      <ul id="eg-leg1" className="legenda active">
        <li><span className="sq ga"></span> Egressos <span className="pr-note"><b>*</b></span></li>
        <li><span className="sq g-dashed">
        <svg preserveAspectRatio="xMinYMin meet"  viewBox="0 0 12 12">
          <rect x="0" y="0" width="12" height="12" fill="url(#diagBar)"></rect>
        </svg>
        </span> Presos</li>
      </ul>
      <div className="chart-info pr-note block"><b>*</b> O JUSTA não conseguiu obter dados sobre egressos no Estado do PR</div>
      <svg id="svg-egress" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
        <defs>
        <pattern id="diagBar" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
          <line x1="1" y="0" x2="1" y2="4" stroke="#666" fillOpacity="0.6" strokeWidth="1.5" />
          <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#fff" fillOpacity="0.6" strokeWidth="1.5" />
        </pattern>
        </defs>
        <pattern id="diagYellow" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
          <line x1="1" y="0" x2="1" y2="4" stroke="#F7D841" fillOpacity="0.6" strokeWidth="1.5" />
        </pattern>
      </svg>
    </div>
    <div className="chart-block">
      <p className="chart-info">O último censo da população prisional feito pelo INFOPEN/DEPEN foi de 2016; não há dados disponíveis para 2017 e 2018.</p>
      <p className="chart-source"><b>Fontes:</b><br/>
        <b>Dados de população prisional:</b> InfoPen 2016 e Anuários do Fórum Brasileiro de Segurança Pública (2015 e 2016) &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b> JUSTA <br></br>
        <b>Dados de egressos prisionais:</b> Poder Executivo de SP e Poder Executivo do CE &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b> JUSTA
      </p>
    </div>
    <div id="eg-topic2" className="chart-block">
      <p>&nbsp;</p>

      <div id="egress-intro-2" className="intro-block">
        <p><span className="title-desc">Gastos com Políticas de Egressos Prisionais</span></p>
        <div className="eg text-block active">
          <p className="active">Valor destinado a políticas de egressos por ano: valores previstos na LOA (lei orçamentária anual) e valores efetivamente gastos.</p>
        </div>
      </div>
      <ul id="eg-leg2" className="legenda active">
        <li><span className="sq ga"></span> Valor liquidado</li>
        <li className="movable"><span className="sq g-dashed">
        <svg preserveAspectRatio="xMinYMin meet"  viewBox="0 0 12 12">
          <rect x="0" y="0" width="12" height="12" fill="url(#diagBar)"></rect>
        </svg>
        </span> Valor previsto (LOA)</li>
      </ul>
      <div>
        <svg id="svg-egress-2" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>

        </svg>
      </div>

      <p className="chart-info">(Valores nominais, como informados pelo Poder Executivo Estadual)</p>
      <p className="chart-info" id="chart-info-ce">Em resposta a pedido de informação o Estado do CE não forneceu dados sobre os valores previstos.</p>
      <p className="chart-info" id="chart-info-sp">As inconsistências dos dados do ano de 2013 impediram sua inclusão na série histórica.</p>
      <p className="chart-source"><b>Fontes:</b><br/>
          <b>Dados de população prisional:</b> InfoPen 2016 e Anuários do Fórum Brasileiro de Segurança Pública (2015 e 2016) &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b> JUSTA <br></br>
          <b>Dados de valores destinados aos egressos prisionais:</b> Poder Executivo de SP e Poder Executivo do CE &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b> JUSTA
      </p>
    </div>
    </section>
  )
}

export default Egress;
