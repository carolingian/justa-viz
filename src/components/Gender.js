import * as d3 from "d3";
import React from 'react'
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';
import './../styles/Header.scss';

import GenderRace from './GenderRace';
import GenderRaceProf from './GenderRaceProf';

function Gender(){
  let w = window.innerWidth,
      h = window.innerHeight*.4;
  let maxH = (h/6)*3,
      halfUp = maxH + 40 - 5,
      halfDown = maxH + 45;
  let xPoint = [];
  let genderData = [];
  let maxPoints = 1;
  let spacing = 20, corner = 0;
  let hF = [], hM = [];
  let visibleParts = 1;
  let figures = [];
  let pathF, pathM;
  let svg;

  function setPoints(nPoints){
    let p=0;
    hF = [];
    hM = [];
    for(var i=0; i<genderData.length; i++){
      hF[i] = genderData[i].mulher*maxH;
      hM[i] = genderData[i].homem*maxH;

      if(p>nPoints-1){
        xPoint[i] = w;
        p = nPoints;
      }else{
        xPoint[i] = (p+1)*(w/(nPoints));
        p++;
      }
    }
    hF.push(genderData[genderData.length-1].mulher*maxH);
    hM.push(genderData[genderData.length-1].homem*maxH);
  }

  function getPathF(nPoints){
    pathF =	"M0,"+halfUp;
    pathF += " L0,"+ (halfUp - hF[0]);

    for(var i=0; i<maxPoints; i++){
      corner = (hF[i+1] - hF[i])/2;
      if(corner > 16){ corner = 16 }
      if(corner < -16){ corner = -16 }
      if(i>nPoints-1){
        corner = 0;
      }

      if(i==maxPoints-1){
        pathF += " L"+(xPoint[i])+","+(halfUp - hF[i]);
      }else{
        pathF += " L"+(xPoint[i] - Math.abs(corner))+","+(halfUp - hF[i]);
        pathF += " S"+(xPoint[i])+","+(halfUp - hF[i])+
                  " "+(xPoint[i])+","+(halfUp - hF[i] - corner);
        pathF += " L"+(xPoint[i])+","+(halfUp - hF[i+1] + corner);
        pathF += " S"+(xPoint[i])+","+(halfUp - hF[i+1])+
                    " "+(xPoint[i] + Math.abs(corner))+","+(halfUp - hF[i+1]);
      }

    }
    pathF += " L"+w+","+ (halfUp - hF[nPoints-1]);
    pathF += " L"+w+","+halfUp+"z";

    return pathF;
  }

  function getPathM(nPoints){
    pathM =	"M0,"+halfDown;
    pathM += " L0,"+ (halfDown + hM[0]);

    for(var i=0; i<maxPoints; i++){
      corner = (hF[i+1] - hF[i])/2;
      if(corner > 16){ corner = 16 }
      if(corner < -16){ corner = -16 }
      if(i>nPoints-1){
        corner = 0;
      }

      if(i==maxPoints-1){
        pathM += " L"+(xPoint[i])+","+(halfDown + hM[i]);
      }else{
        pathM += " L"+(xPoint[i] - Math.abs(corner))+","+(halfDown + hM[i]);
        pathM += " S"+(xPoint[i])+","+(halfDown + hM[i])+
                  " "+(xPoint[i])+","+(halfDown + hM[i] - corner);
        pathM += " L"+(xPoint[i])+","+(halfDown + hM[i+1] + corner);
        pathM += " S"+(xPoint[i])+","+(halfDown + hM[i+1])+
                    " "+(xPoint[i] + Math.abs(corner))+","+(halfDown + hM[i+1]);
      }
    }
    pathM += " L"+w+","+ (halfDown + hM[nPoints-1]);
    pathM += " L"+w+","+halfDown+"z";

    return pathM;
  }

  function draw(num){
    w = Number(svg.style("width").slice(0, -2));
    h = window.innerHeight*0.6;//svg.style("height").slice(0, -2);
    if(window.innerHeight > window.innerWidth){
      h = window.innerHeight*0.4;
    }
    if(h < 480){ h = 480 }
    if(h > 540){ h = 540 }
    svg.attr("viewBox", "0 0 "+ w +" "+h);
    maxH = (h/6)*3;
    halfUp = (h/3) + 40 - 5,
    halfDown = (h/3) + 45;

    setPoints(visibleParts);

    if(num){
      visibleParts = num;
      d3.select(".genderGraphF").attr("d", getPathF(visibleParts));
      d3.select(".genderGraphM").attr("d", getPathM(visibleParts));
    }else{
      d3.select(".genderGraphF").transition().duration(500).attr("d", getPathF(visibleParts));
      d3.select(".genderGraphM").transition().duration(500).attr("d", getPathM(visibleParts));
    }

    if(figures.length == 0){
      drawFigures(visibleParts);
    }else{
      moveFigures(visibleParts);
    }
  }

  function drawFigures(nPoints){
    let fg;
    for(var i=0; i<genderData.length; i++){
      fg = svg.append("g").attr("id", "fig_"+i).attr("class", "fig");

      // Female
      fg.append("text")
        .attr("class", "figNumber figF active")
        .attr("y", -hF[i] - (h/30) - 5)
        .text(Math.round(genderData[i].mulher*100)+"%");
      fg.append("text")
        .attr("class", "figNumber figF active span")
        .attr("x", 83)
        .attr("y", -hF[i] - (h/30) -15)
        .text(" mulheres");

      // Male
      fg.append("text")
        .attr("class", "figNumber figM active")
        .attr("y", hM[i] + (h/30) + 30)
        .text(Math.round(genderData[i].homem*100)+"%");
      fg.append("text")
        .attr("class", "figNumber figM active span")
        .attr("x", 83)
        .attr("y", hM[i] + (h/30) + 20)
        .text(" homens");

      // label
      let tg = fg.append("rect")
        .attr("class", "figTag");

      fg.append("text")
        .attr("id", "gLabel-"+i)
        .attr("class", "figLabel")
        .attr("x", -10)
        .attr("y", 4)
        .attr("font-family", "Raleway")
        .attr("font-weight", "400")
        .attr("font-size", "11px")
        .text(genderData[i].descricao.toUpperCase());

      tg.attr("y", -10)
        .attr("width", function(){
          return d3.select("#gLabel-"+i).node().getBBox().width + 20;
        })
        .attr("x", -20)
        .attr("height", 20);

      fg.append("rect")
        .attr("class", "figBullet figM")
        .attr("x", -12.5)
        .attr("width", 5)
        .attr("height", 5)
        .attr("y", hM[i] + (h/30) + 15);

      fg.append("rect")
        .attr("class", "figBullet figF")
        .attr("x", -12.5)
        .attr("width", 5)
        .attr("height", 5)
        .attr("y", -hF[i] - (h/30) - 15);

      fg.append("line")
        .attr("class", "figLine figF")
        .attr("x1", -10)
        .attr("x2", -10)
        .attr("y1", -hF[i] - (h/30) - 15 )
        .attr("y2", -30);

      fg.append("line")
        .attr("class", "figLine figM")
        .attr("x1", -10)
        .attr("x2", -10)
        .attr("y1", hM[i] + (h/30) + 15)
        .attr("y2", 30);


      figures[i] = fg;

      if(i>0){
        figures[i].attr("transform", "translate("+(xPoint[i-1]+30)+","+(halfUp+5)+")");
      }else{
        figures[i].attr("transform", "translate(30,"+(halfUp+5)+")");
      }
    }
    figures[0].classed("active", true);

  }

  function moveFigures(nPoints){
    for(var i=0; i<genderData.length; i++){
      if(i>0){
        figures[i].transition().duration(500)
        .attr("transform", "translate("+(xPoint[i-1]+30)+","+(halfUp+5)+")");
      }else{
        figures[i].transition().duration(500)
        .attr("transform", "translate(30,"+(halfUp+5)+")");
      }


      figures[i].selectAll(".figM").transition().duration(100)
        .attr("y", hM[i] + (h/30) + 30);
      figures[i].selectAll(".figF").transition().duration(100)
        .attr("y", -hF[i] - (h/30) - 5);

      figures[i].selectAll(".figM.active").transition().duration(100)
        .attr("y", hM[i] + (h/30) + 30);

      figures[i].selectAll(".figM.span").transition().duration(100)
        .attr("y", hM[i] + (h/30) + 20);
      figures[i].selectAll(".figF.span").transition().duration(100)
        .attr("y", -hF[i] - (h/30) - 15);

      figures[i].selectAll(".figF.figLine").transition().duration(100)
          .attr("y1", -hF[i] - (h/30) - 15 );
      figures[i].selectAll(".figM.figLine").transition().duration(100)
          .attr("y1", hM[i] + (h/30) + 15);

      figures[i].selectAll(".figF.figBullet").transition().duration(100)
          .attr("y", -hF[i] - (h/30) - 15 );
      figures[i].selectAll(".figM.figBullet").transition().duration(100)
          .attr("y", hM[i] + (h/30) + 15);

      if(i != nPoints-1){
        figures[i].selectAll(".figNumber").classed("active", false);
      }else{
        figures[i].selectAll(".figNumber").classed("active", true);
      }

      if(i <= nPoints-1){
        figures[i].classed("active", true);
      }else{
        figures[i].classed("active", false);
      }
    }
  }

  window.addEventListener("resize", function(){
    draw(visibleParts);
  });
  let initTimeout = setTimeout(setup, 100);

  function setup(){
    svg = d3.select("#svg-gender");

    // load national gender data
    d3.csv("data/gender-br.csv")
    .then(function(data) {
        genderData = data;
        maxPoints = genderData.length;

        draw(1);
    })
    .catch(function(error){
       console.log("error on gender-br");
    });

    d3.selectAll("#next-gender,#last-gender").on("click", function(d){
      let isNotLast = visibleParts < maxPoints;
      let increment = 1;
      let initPart = 1;

      if(d3.select(this).attr("id")=="last-gender"){
        isNotLast = visibleParts > 1;
        increment = -1;
        initPart = maxPoints;
      }

      if(isNotLast){
        visibleParts += increment;
        draw();

        if(w > window.innerWidth){
          var scrollW = d3.select("#svg-gr-container").property("scrollWidth") - window.innerWidth;
          var diffX = (visibleParts-1)*scrollW;
          var elemPosX = (((visibleParts-1)/(maxPoints-1))*scrollW)+window.innerWidth/2;

          if(visibleParts==1){
            elemPosX = 0;
          }

          d3.select("#svg-gr-container").transition().duration(1000)
            .tween("svgTween1", scrollLeftTween(elemPosX));
        }

      }else{
        /*
        visibleParts = initPart;

        if(w > window.innerWidth){
          var scrollparcel = d3.select("#svg-gr-container").property("scrollWidth")/(maxPoints-1);
          var diffX = (visibleParts-1)*scrollparcel;

          d3.select("#svg-gr-container").transition().duration(500)
            .tween("svgTween1", scrollLeftTween(diffX));
        }
        draw();
        */
      }

      let stepper = d3.select("#gender-stepper");
      stepper.style("opacity", 0.7);
      stepper.selectAll("li").each(function(d, i){
        if(i!=visibleParts){
          d3.select(this).classed("active", false);
          d3.select("#gender-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", false);
        }else{
          d3.select(this).classed("active", true);
          d3.select("#gender-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", true);
        }
      })
    });
  }

  function scrollLeftTween(scrollLeft) {
    return function() {
      var i = d3.interpolateNumber(this.scrollLeft, scrollLeft);
      return function(t) { this.scrollLeft = i(t); };
    }
  };

  return (
    <section id="gender-race">
    <div className="chart-block-wide">

      <div id="gender-intro" className="intro-block padded">
        <h3>Desigualdades na Magistratura
        <span className="title-desc block">Juízes e Desembargadores</span>
        </h3>

        <ul id="gender-stepper" className="dot-stepper">
          <li><a id="last-gender" className="arrow-last"></a></li>
          <li className="active"><a className="dot"></a></li>
          <li><a className="dot"></a></li>
          <li><a className="dot"></a></li>
          <li><a className="dot"></a></li>
          <li><a id="next-gender" className="arrow-next pulse"></a></li>
        </ul>

        <div className="text-block">
          <p className="active">Na <b>população brasileira</b>, a proporção entre os dois sexos é praticamente equilibrada</p>
          <p>A proporção de mulheres cresce entre as pessoas <b>com ensino superior</b></p>
          <p>No entanto, entre as pessoas que ocupam <b>cargos de juízes</b>, a maioria é do sexo masculino</p>
          <p>E a desigualdade é ainda maior na progressão da carreira para os cargos de <b>desembargadores</b></p>
        </div>
        <p className="chart-source"><b>Fonte:</b> CNJ 2018 e Censo IBGE 2010
        &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b> JUSTA
        </p>
        <p>&nbsp;</p>
      </div>
      <div className="svg-container" id="svg-gr-container">
        <svg id="svg-gender" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+ w +" "+h}>
        <defs>
        <pattern id="diagonalF" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
          <line x1="1" y="0" x2="1" y2="4" stroke="#000" strokeWidth="1.5" />
        </pattern>
        <pattern id="diagonalM" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
          <line x1="1" y="0" x2="1" y2="4" stroke="#000" strokeWidth="1.5" />
        </pattern>
        </defs>

        <path className="genderGraphF" fill={ "url("+ window.location.href + "#diagonalF)"} d="" />
        <path className="genderGraphM" fill={ "url("+ window.location.href + "#diagonalM)"} d="" />

        </svg>
      </div>
    </div>

    <GenderRace></GenderRace>
    <GenderRaceProf></GenderRaceProf>
    </section>
  )
}

export default Gender;
