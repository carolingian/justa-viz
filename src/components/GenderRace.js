import * as d3 from "d3";
import React from 'react'
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';
import './../styles/Header.scss';

function GenderRace(){
  let w = window.innerWidth - 20,
      h = window.innerHeight*0.8;
  let svg, svgH, svgW;
  let groupM, groupF;
  let margin = {top: 30, right: 20, bottom: 0, left: 40};
  let grData = {};
  let descriptors = ["População", "Magistrados", "Desembargadores"];
  let x, x2, y, z;
  let keys = [["np","bp","ip","ap"], ["nm","bm","im","am"]];
  let animInterval;
  let currAnim=0;
  let currStep = 1;
  let maxSteps = 2;
  let tooltip;

  function setup(){
    svg = d3.select("#svg-gr-1");

    // load national gender data
    d3.tsv("data/gender-race-uf.tsv", function(d,i,cols){
      var t1=0, t2=0;

      for (i = 2; i < cols.length; ++i){
        d[cols[i]] = +d[cols[i]]
      }
      for (i = 0; i < keys[0].length; ++i){
        t1 += Number(d[keys[0][i]]);
        t2 += Number(d[keys[1][i]]);
      }
      d.totals = [t1,t2];

      return d;
    })
    .then(function(data) {

      grData.m = data.filter(function(d,i){
        return d.sexo == "Homens";
      }).sort(function(a, b) {
        return a.bp - b.bp;
      });

      grData.f = data.filter(function(d,i){
        return d.sexo == "Mulheres";
      });

      mapOrder(grData.f, grData.m, "bp");

      draw();
    })

    d3.selectAll("#next-gr,#last-gr").on("click", function(d){
      let isNotLast = currStep < maxSteps;
      let increment = 1;
      let initPart = 1;

      if(d3.select(this).attr("id")=="last-gr"){
        isNotLast = currStep > 1;
        increment = -1;
        initPart = maxSteps;
      }

      if(isNotLast){
        currStep += increment;
        //draw();
      }else{
        //draw();
      }

      animateChange();

      let stepper = d3.select("#gr-stepper");
      stepper.style("opacity", 0.7);
      stepper.selectAll("li").each(function(d, i){
        if(i!=currStep){
          d3.select(this).classed("active", false);
          d3.select("#gr-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", false);
        }else{
          d3.select(this).classed("active", true);
          d3.select("#gr-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", true);
        }
      })
    });
  }

  function mapOrder(array, order, key) {
    array.sort( function (a, b) {
      var A = a[key], B = b[key];

      if (order.indexOf(A) > order.indexOf(B)) {
        return 1;
      } else {
        return -1;
      }
    });
    return array;
  };


  function draw(){
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }


    w = Number(svg.style("width").slice(0,-2));
    h = 28*24;//window.innerHeight*0.8;
    /*if(window.innerHeight > window.innerWidth){
      h = window.innerHeight*0.6;
    }
    if(h < 480){
      h = 480;
    }*/
    svgH = h - margin.top - margin.bottom;
    svgW = w;
    svg.attr("viewBox", "0 0 "+ w +" "+h);

    //svg.html("");

    x = d3.scaleLinear()
    .range([0,(svgW-60)]);

    x2 = d3.scaleLinear()
    .range([(svgW-60),0]);


    y = d3.scaleBand()
    .rangeRound([0, svgH])
    .paddingInner(0.05)
    .align(0.1);

    // set the colors
    z = d3.scaleOrdinal()
    .range(["#b33040", "#d25c4d", "#f2b447", "#d9d574"]);

    y.domain(grData.m.map(function(d) { return d.uf; }));
    x.domain([0, 1]);
    x2.domain([0, 1]);
    z.domain(keys[0]);

    svg.selectAll(".gr-group,.gr-line").remove();
    //svg.select("gr-bar").remove();

    groupM = svg.append("g").attr("class","gr-group");
    groupM.attr("transform","translate(0,"+margin.top+")");

    var barsM = groupM.selectAll("g.gr-bar")
      .data(grData.m)
      .enter().append("g")
      .attr("class","gr-bar");

    barsM.append("text")
        .attr("x",0)
        .attr("y", function(d) {
          return y(d.uf) + ((svgH-(27*3))/27) - 2;
        })
        .attr("fill", "#fff")
        .attr("font-size", "11px")
        .attr("fill-opacity", "0.45")
        .text(function(d) {
          return d.uf;
        });

    // barras do gráfico - sexo masculino
    let bMg = barsM.selectAll(".gr-bar")
      .data(function(d) {
        let t = [
          [d.np, d.nm], [d.bp, d.bm], [d.ip, d.im], [d.ap, d.am]
        ]
        let obj = [
                  {v:[d.np, d.nm], x:[0,0], uf:d.uf, label:"gn", totals:d.totals, data: t},
                  {v:[d.ip, d.im], x:[d.np,d.nm], uf:d.uf, label:"gi", totals:d.totals, data: t},
                  {v:[d.ap, d.am], x:[d.ip+d.np,d.im+d.nm], uf:d.uf, label:"ga", totals:d.totals, data: t},
                  {v:[d.bp, d.bm], x:[d.ip+d.np+d.ap,d.im+d.nm+d.am], uf:d.uf, label:"gb", totals:d.totals, data: t}
                ];

        return obj;
      })
      .enter().append("g")
      .attr("class", "gm")
      .on("mouseover", showTooltip)
      .on("mouseout", function(){
        tooltip.classed("active", false)
      });

    bMg.append("rect")
      .attr("rx", 2)
      .attr("ry", 2)
      .attr("class", function(d) {
        let l = d.label;
        return "gr-rect "+l;
      })
      .attr("height", ((svgH-(27*3))/27)-2)
      .attr("width", function(d) {
        return x(d.v[0]/1);
      })
      .attr("data-v", function(d) {
        return d.v[0];
      })
      .attr("x", function(d) {
        return 28 + x(d.x[0]/1);
      })
      .attr("y", function(d) {
        return y(d.uf);
      });

    groupF = svg.append("g").attr("class","gr-group");
    groupF.attr("transform","translate(0,"+margin.top+")");

    let barsF = groupF.selectAll("g.gr-bar")
      .data(grData.f)
      .enter().append("g")
      .attr("class","gr-bar");

    barsF.append("text")
        .attr("x", w)
        .attr("y", function(d) {
          return y(d.uf) + ((h-(27*3))/27) - 2;
        })
        .attr("fill", "#fff")
        .attr("text-anchor", "end")
        .attr("font-size", "11px")
        .style("opacity", "0.45")
        .text(function(d) {
          return d.uf;
        });

    let bFg = barsF.selectAll("g.gr-bar")
      .data(function(d) {
        let t = [
          [d.np, d.nm], [d.bp, d.bm], [d.ip, d.im], [d.ap, d.am]
        ]
        let obj = [
                  {v:[d.np, d.nm], x:[d.np,d.nm], uf:d.uf, label:"gn", totals:d.totals, data: t},
                  {v:[d.ip, d.im], x:[d.np+d.ip,d.nm+d.im], uf:d.uf, label:"gi", totals:d.totals, data: t},
                  {v:[d.ap, d.am], x:[d.ip+d.np+d.ap,d.im+d.nm+d.am], uf:d.uf, label:"ga", totals:d.totals, data: t},
                  {v:[d.bp, d.bm], x:[d.ip+d.np+d.ap+d.bp,d.im+d.nm+d.am+d.bm], uf:d.uf, label:"gb", totals:d.totals, data: t}
                ];
        return obj;
      })
      .enter().append("g")
      .attr("class", "gw")
      .on("mouseover", showTooltip)
      .on("mouseout", function(){
        tooltip.classed("active", false)
      });

    bFg.append("rect")
      .attr("rx", 2)
      .attr("ry", 2)
      .attr("class", function(d) {
        let l = d.label;
        return "gr-rect "+l;
      })
      .attr("height", ((svgH-(27*3))/27)-2)
      .attr("width", function(d) {
        return x(d.v[0]/1);
      })
      .attr("x", function(d) {
        return x2(d.x[0]/1) + 32;
      })
      .attr("y", function(d) {
        return y(d.uf);
      });

    currAnim = 0;
    //animInterval = setInterval(animateChange, 3000);

    let fiftyLine1 = svg.append("line")
    .attr("class","gr-line")
    .attr("x1", w/2)
    .attr("x2", w/2)
    .attr("y1", 15)
    .attr("y2", 28)
    .attr("stroke","#dedede")
    .attr("stroke-width","2")
    .attr("opacity",.65);

    let fiftyText = svg.append("text")
    .attr("class","gr-line")
    .attr("x", w/2 - 8)
    .attr("y", 10)
    .attr("fill","#fff")
    .style("font-size","10px")
    .style("font-family","Helvetica")
    .text("50%")
    .attr("opacity",.65);

    let fiftyLine2 = svg.append("line")
    .attr("class","gr-line")
    .attr("x1", w/2)
    .attr("x2", w/2)
    .attr("y1", h - 20)
    .attr("y2", h)
    .attr("stroke","#dedede")
    .attr("stroke-width","2")
    .attr("opacity",.65);
  }

  function positionTooltip(){
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};

    let offset = {
        x: d3.select("#tooltip").style("width").slice(0,-2) / 2,
        y: d3.select("#tooltip").style("height").slice(0,-2) / 2
    }
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y - 30;

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function toPercentStr(v){
    let num = (v*100).toFixed(1);

    return num + "%";
  }

  function showTooltip(d){
    let data = d.data;
    let title = "Homens";
    let str = "";

    if(!data){ return; }
    tooltip.classed("active", true);

    if(d3.select(this).classed("gw")){ title = "Mulheres" }

    str += title + "<br/>";
    for(var i=0; i<data.length;i++){
      str += "<span class='sq "+("g"+keys[0][i].substr(0,1))+"'></span> "+ toPercentStr(data[i][currStep-1]);
    }

    tooltip.html(str);
    positionTooltip();
  }

  function animateChange(){
    let index = currStep - 1;

    if(index < 2){
      groupM.style("visibility", "visible");
      groupF.style("visibility", "visible");

      groupM.selectAll(".gr-rect")
      .transition().duration(500)
      .attr("width", function(d) {
        return x(d.v[index]/1);
      })
      .attr("x", function(d) {
        return 28 + x(d.x[index]/1);
      })

      groupF.selectAll(".gr-rect")
      .transition().duration(500)
      .attr("width", function(d) {
        return x(d.v[index]/1);
      })
      .attr("x", function(d) {
        return x2(d.x[index]/1) + 32;
      })
    }else{
      groupM.style("visibility", "hidden");
      groupF.style("visibility", "hidden");
    }

    d3.select("#title-desc1")
      .text(descriptors[index]);
  }

  window.addEventListener("resize", function(){
    draw();
  });
  let initTimeout = setTimeout(setup, 100);

  return (
      <div className="chart-block">
        <h3>Distribuição por cor/raça e gênero</h3>
        <div id="gr-intro" className="intro-block">
          <ul id="gr-stepper" className="dot-stepper">
            <li><a id="last-gr" className="arrow-last"></a></li>
            <li className="active"><a className="dot"></a></li>
            <li><a className="dot"></a></li>
            <li><a id="next-gr" className="arrow-next pulse"></a></li>
          </ul>
          <div className="text-block">
            <p className="active">A proporção da população por gênero é equilibrada em todo o país, embora a distribuição racial seja diferente dependendo da Unidade Federativa</p>
            <p>Entre os magistrados, no entanto, a maior parte é de homens brancos em quase todos os estados</p>
            </div>
        </div>

        <ul id="gr-leg1" className="legenda active">
          <span className="title-desc" id="title-desc1">População</span>
          <li><span className="sq gn"></span> Negros</li>
          <li><span className="sq gb"></span> Brancos</li>
          <li><span className="sq gi"></span> Indígenas</li>
          <li><span className="sq ga"></span> Amarelos</li>
        </ul>
        <div id="gender-race-labels">
            <div>Homens</div>
            <div className="right">Mulheres</div>
        </div>
        <svg id="svg-gr-1" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+w+" "+h+""}>
          <defs>
            <pattern id="dotted" patternUnits="userSpaceOnUse" height="4" width="4">
              <rect fill="#fff" width="1" height="1" x="1" y="1"></rect>
              <rect fill="#fff" width="1" height="1" x="3" y="3"></rect>
            </pattern>
          </defs>
          <rect fill="url(#dotted)" width="200"></rect>
        </svg>
        <div className="chart-block no-padding">
          <p className="chart-source">
          <b>Fonte:</b> CNJ 2018 e Censo IBGE 2010<br/>
          <b>Elaboração:</b> JUSTA
          </p>
          <p>&nbsp;</p>
        </div>
      </div>
  )
}

export default GenderRace;
