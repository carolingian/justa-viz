import React from 'react';
import * as d3 from "d3";
import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';
import './../styles/Header.scss';
import './../styles/Main.scss';

function Header(props){
  let dropdown, dropdownLink, openMenuBt, closeMenuBt;

  function setup(){
    dropdownLink = d3.selectAll("nav .dropdown");

    dropdownLink.on("mouseover", function(event){
      d3.select(this).select(".dropdown-menu").classed("active", true);
    });

    dropdownLink.on("mouseout", function(event){
      d3.select(this).select(".dropdown-menu").classed("active", false);
    });

    openMenuBt = d3.select("#open-menu");
    openMenuBt.on("click", function(event){
      d3.select("#primary-menu .menu-title").classed("active", true);
      d3.select("#primary-menu .nav-bar").classed("active", true);
      d3.select("#page-content")
        .transition().delay(0).duration(500)
        .style("margin-left", "50%")
    });

    closeMenuBt = d3.select("#close-menu");
    closeMenuBt.on("click", function(event){
      d3.select("#primary-menu .menu-title").classed("active", false);
      d3.select("#primary-menu .nav-bar").classed("active", false);
      d3.select("#page-content")
        .transition().delay(100).duration(500)
        .style("margin-left", "0")
    });

  }

  let initTimeout = setTimeout(setup, 200);

    //<Parallax className={ "parallax-header " } offsetYMin={'-75%'} offsetYMax={'75%'} slowerScrollRate>
      //<div className="parallax-image" style={{ backgroundImage: `url(${props.bg})` }} />
    //</Parallax>
  return (
    <section className="header">
      <div id="site-name" className={ props.logoVisibility }><Link to={ "/" } className="logo"><img src="./images/logo-justa-w.png"/></Link></div>

      <nav id="primary-menu">
        <button id="open-menu">
          <svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 45 45">
            <rect x="14" y="18" fill="#000000" width="16" height="2"/>
            <rect x="14" y="23" fill="#000000" width="16" height="2"/>
            <rect x="14" y="28" fill="#000000" width="16" height="2"/>
          </svg>
        </button>
        <ul className="nav-bar">
          <span className="menu-title">
            <button id="close-menu">x</button> Menu
          </span>
          <li id="menu-item-8705" className="dropdown menu-item-8705 level-0"><a href="http://justa.org.br/o-justa/" className="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">O Justa <b className="caret"></b></a>
          <ul className="dropdown-menu">
          	<li id="menu-item-8709" className="menu-item-8709 level-1"><a href="http://justa.org.br/o-justa/">Entenda o Justa</a></li>
          	<li id="menu-item-8708" className="menu-item-8708 level-1"><a href="http://justa.org.br/o-justa/quem-somos/">Quem Somos</a></li>
          	<li id="menu-item-8707" className="menu-item-8707 level-1"><a href="http://justa.org.br/o-justa/o-que-fazemos/">O que fazemos</a></li>
          	<li id="menu-item-9267" className="menu-item-9267 level-1"><a href="http://justa.org.br/o-justa/nosso-compromisso/">Nosso Compromisso</a></li>
          	<li id="menu-item-9266" className="menu-item-9266 level-1"><a href="http://justa.org.br/o-justa/nossos-dados/">Nossos Dados</a></li>
          	<li id="menu-item-8706" className="menu-item-8706 level-1"><a href="http://justa.org.br/o-justa/faq/">FAQ</a></li>
          </ul>
          </li>
          <li id="menu-item-9426" className="menu-item-9426 level-0"><a href="http://justa.org.br/nossasanalises/">Nossas Análises</a></li>
          <li id="menu-item-9388" className="menu-item-9388 level-0"><a href="/dados">Acesse os dados</a></li>
          <li id="menu-item-8756" className="dropdown menu-item-8756 level-0"><a href="http://justa.org.br/noticias/" className="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Notícias <b className="caret"></b></a>
          <ul className="dropdown-menu">
          	<li id="menu-item-9372" className="menu-item-9372 level-1"><a href="http://justa.org.br/category/imprensa/">Imprensa</a></li>
          	<li id="menu-item-9371" className="menu-item-9371 level-1"><a href="http://justa.org.br/category/ultimas-noticias/">Últimas Notícias</a></li>
          </ul>
          </li>
          <li id="menu-item-9113" className="menu-item-9113 level-0"><a href="http://justa.org.br/#contato">Contato</a></li>
          </ul>
      </nav>

      <div className="chart-block top">
      <p>Explore alguns dos dados do JUSTA na ferramenta de visualização abaixo.
Para conhecer a metodologia de pesquisa, acesse o link <a href="http://justa.org.br/nossasanalises/" title="Metodologia">Nossas Análises</a>.</p>
      </div>
    </section>
  )
}

export default Header
