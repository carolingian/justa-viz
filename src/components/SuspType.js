import * as d3 from "d3";
import React from 'react'
import Select from './custom.select';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function SuspType(props){
  let w = window.innerWidth - 20,
      h = window.innerHeight*0.6;
  let svg, xScale, yScale, yScale2, xAxis, yAxis;
  let vis;
  let gX, gY, svgH, svgW;
  let margin = {top: 5, right: 10, bottom: 60, left: 170};
  let susData = [], ygData = {};
  let fontSize = 12;
  let yearGroupData;
  let currVars = {state:0, total:0};
  let vars = {states: ["ce", "pr", "sp"],
              stateTexts: ["do CE", "do PR", "de SP"],
              access: ["Inacessível", "Segredo de Justiça"],
              years: ["2018","2017","2016","2015","2014","2013"]};
  let tooltip, selectors;

  function setup(){
    svg = d3.select("#svg-sus-2");
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip");
    }

    selectors = document.querySelector("#sus").querySelectorAll(".select");
    selectors[0].onchange = changeSelections;
    //selectors = [];
    /*customSelects.forEach(function(e,i){
      selectors.push( new Select({target: e, callback: changeSelections}) );
    });*/


    // load process data
    d3.tsv("data/suspensao.tsv")
    .then(function(data) {
      let nested = d3.nest()
        .key(function(d){ return d.uf.toLowerCase(); })
        .key(function(d){ return d.assunto.toLowerCase(); })
        .rollup(function(v){
          let obj = {}
          let decision1List, decision2List;
          decision1List = v.filter(function(d){
            return d.esfera.toLowerCase() == "estadual" && d.decisao.toLowerCase().indexOf("acolhido") > -1;
          });
          decision2List = v.filter(function(d){
            return d.esfera.toLowerCase() == "estadual" && d.decisao.toLowerCase().indexOf("negado") > -1;
          });

          obj = {
            acolhidos: decision1List.length,
            negados: decision2List.length,
            assunto: v[0].assunto,
            total: decision1List.length + decision2List.length,
            list: v
          }

          return obj;
        })
        .entries(data);

        // console.log(nested);
        susData = []
        nested.forEach(function(d,i){
          let list = [];
          d.values.forEach(function(dd,ii){
            if(dd.value.total > 0){
              list.push(dd.value);
            }
          })

          list.sort(function(a,b){
            if (a.total > b.total) return -1;
            if (b.total > a.total) return 1;
            return 0;
          });

          susData[d.key] = list;
        });

        //console.log(susData);
        draw(true);
      })
  }

  function showTooltip(d){
    let total = '';
    let access = ["acolhidos", "negados"]
    let accessName = ["suspensos", "mantidos"]
    let type;

    type = d3.select(this).attr("class").split("bar")[1];
    total = d[access[type-1]] + " processos " + accessName[type-1];

    //total = d.total + ' processos';

    let str = total;

    tooltip.classed("active", true);
    tooltip.html(str);

    positionTooltip();
  }

  function positionTooltip(){
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};

    let offset = {
        x: d3.select("#tooltip").style("width").slice(0,-2) / 2,
        y: d3.select("#tooltip").style("height").slice(0,-2) / 2
    }
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y;

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function draw(redraw){
    let currData = susData[vars.states[currVars.state]];
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    if(redraw){
      w = Number(svg.style("width").slice(0,-2));
      h = currData.length*45;//window.innerHeight*0.6;

      if(w < 480){
        margin.left = 145;
        fontSize = 10;
      }else{
        margin.left = 170;
        fontSize = 12;
      }

      svgH = h - margin.top - margin.bottom;
      svgW = w - margin.left - margin.right;

      svg.attr("viewBox", "0 0 "+ w +" "+h);
    }

    buildVis(redraw);
  }


  function changeSelections(){


    let index = selectors[0].selectedIndex;
    console.log("ra");
    /*
    let sel = selectors[0].node;
    let selStr = sel.options[sel.selectedIndex].value;
    let list = sel.parentNode.getAttribute("id").split("-")[2];
    let index = vars.states.indexOf(selStr.toLowerCase());
    */

    currVars.state = index;

    draw(true);
  }

  function buildVis(redraw){
    let currData = susData[vars.states[currVars.state]];
    let total = d3.sum(currData, function(d){
      return +d.total;
    })

    let duration = 500;
    if(redraw){ duration = 0; }

    let maxX = d3.max(currData, function(d){
      return d.total;
    });

    svg.selectAll(".axis").remove();
    svg.selectAll("#sus2-vis").remove();
    vis = svg.append("g").attr("id", "sus2-vis");
    //vis.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    yScale = d3.scaleBand()
    .domain(currData.map(function(d){ return d.assunto; }))
    .range([0, svgH])
    .padding(0.1);

    xScale = d3.scaleLinear()
    .domain([0, maxX])
    .range([0, svgW])
    .nice(4);

    xAxis = d3.axisBottom(xScale).ticks(4);
    yAxis = d3.axisLeft(yScale).tickPadding(10);

    gX = svg.append("g")
      .attr("class", "axis axis-x")
      .attr("transform", "translate(" + margin.left + "," + (h - margin.bottom) + ")")
      .call(xAxis);

    gY = svg.append("g")
      .attr("class", "axis axis-y")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(yAxis);

    gY.selectAll(".tick text")
      .attr("font-size", fontSize+"px")
      .attr("text-anchor", "end")
      .attr("x", -10)//margin.left)
      .call(textWrap, margin.left - 10);

    gY.selectAll(".domain, line").remove();

    svg.append("text")
      .attr("class", "axis-title")
      .text("Número de processos")
      .attr("text-anchor","start")
      .attr("x", margin.left)
      .attr("y", svgH + margin.top + 35)

    let group1 = vis.append("g").attr("id", ".sus-v1");

    let bars = group1.selectAll(".sus2-left")
    .data(currData)
    .enter()
    .append("g")
    .attr("class", "sus2-left")
    .attr("transform","translate("+margin.left+"," + margin.top + ")");

    bars.append("rect")
    .attr("class", "sus2-bar2")
    .attr("height", yScale.bandwidth()-4)
    .attr("fill", "#D33145")
    .attr("rx", "3").attr("ry", "3")
    .attr("width", function(d){
      return xScale(d.total);
    })
    .attr("y",function(d,i){
      return yScale(d.assunto);
    })
    .on("mouseover", showTooltip)
    .on("mousemove", positionTooltip)
    .on("mouseout", function(d){ tooltip.classed("active", false) });

    bars.append("rect")
    .attr("class", "sus2-bar1")
    .attr("height", yScale.bandwidth()-4)
    .attr("fill", "#F7D841")
    .attr("rx", "3").attr("ry", "3")
    .attr("width", function(d){
      return xScale(d.acolhidos);
    })
    .attr("y",function(d,i){
      return yScale(d.assunto);
    })
    .on("mouseover", showTooltip)
    .on("mousemove", positionTooltip)
    .on("mouseout", function(d){ tooltip.classed("active", false) });

  }

  function textWrap(text, width) {
    text.each(function() {
      var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = fontSize + 3, // ems
          y = text.attr("y") - 5,
          dy = parseFloat(text.attr("dy")),
          dx = parseFloat(text.attr("x")),
          tspan = text.text(null).append("tspan").attr("x", dx).attr("y", y).attr("dy", dy + "px");
      while (word = words.pop()) {
        line.push(word);
        tspan.text(line.join(" "));
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
          tspan = text.append("tspan").attr("text-anchor", "end").attr("x", dx).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "px").text(word);
        }
      }
    });
  }

  window.addEventListener("resize", function(){
    draw(true);
    //console.log(props)
    //animateChange();
  });
  let initTimeout = setTimeout(setup, 100);

  return (
      <div id="sus2" className="chart-block">
        <ul id="sus2-leg2" className="legenda active">
          <span className="title-desc block">Decisões mantidas e suspensas, por assunto, na esfera estadual</span>
          <li><span className="sq ga"></span> Suspensas</li>
          <li><span className="sq gn"></span> Mantidas</li>
        </ul>
        <svg id="svg-sus-2" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+w+" "+h+""}>
        </svg>
        <p className="chart-source">
          <b>Fonte:</b> Ceará - TJCE / Paraná - TJPR / São Paulo - TJSP &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b>  JUSTA
        </p>
      </div>
  )
}

export default SuspType;
