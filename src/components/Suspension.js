import * as d3 from "d3";
import React from 'react'
import Select from './custom.select';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';
import SuspType from './SuspType';

function Suspension(){
  let w = window.innerWidth - 20,
      h = window.innerHeight*0.7;
  let wB = window.innerWidth - 20,
      hB = window.innerHeight*0.6;
  let svg, svg2;
  let xScale, yScale, yScale2, xAxis, yAxis;
  let xScaleB, yScaleB, xAxisB, yAxisB;
  let vis1, vis2;
  let gX, gY, svgH, svgW;
  let gXB,gYB,svgHB, svgWB;
  let margin = {top: 20, right: 10, bottom: 60, left: 45};
  let marginB = {top: 5, right: 10, bottom: 60, left: 170};
  let susData = {}, susDataB = {}, ygData = {};
  let yearGroupData;
  let currStep = 1;
  let currVars = {state:0, total:0};
  let maxSteps = 4;
  let fontSize = 12;
  let vars = {states: ["ce", "pr", "sp"],
              stateTexts: ["do CE", "do PR", "de SP"],
              access: ["Inacessível", "Segredo de Justiça"],
              years: ["2018","2017","2016","2015","2014","2013"]};
  let colors = [
    "#F7D841",
    "#F7D841",
    "#D33145",
    "#D33145",
  ];
  let tooltip, selectors=[];

  function setup(){
    svg = d3.select("#svg-sus-1");
    svg2 = d3.select("#svg-sus-2");

    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip");
    }

    let customSelects = document.querySelector("#sus").querySelectorAll(".select");

    selectors = [];
    customSelects.forEach(function(e,i){
      selectors.push( new Select({target: e, callback: changeSelections}) );
    });

    // load data
    d3.tsv("data/suspensao-gestao.tsv")
    .then(function(dataG) {
      //year order
      dataG.sort(function(a,b){
        if (a.anos > b.anos) return 1;
        if (b.anos > a.anos) return -1;
        return 0;
      });
      let nested2 = d3.nest()
        .key(function(d){ return d.uf.toLowerCase(); })
        .entries(dataG);

      nested2.forEach(function(d,i){
        ygData[d.key] = d;
      });
      // load process data
      d3.tsv("data/suspensao.tsv")
      .then(function(data) {
        let nested = d3.nest()
          .key(function(d){ return d.uf.toLowerCase(); })
          .rollup(function(v){
            let obj = [];
            let filtered, sphere1List, sphere2List;
            let decision1List, decision2List;
            let index, groupId, groupInfo, groupPos;
            let arrAnos;

            vars.years.forEach(function(t,i){
              index = vars.years.length - i - 1;

              dataG.forEach(function(dg, ii){
                arrAnos = dg.anos.split(",");
                if(arrAnos.indexOf(vars.years[i]) > -1 &&
                  dg.uf.toLowerCase() == v[0].uf.toLowerCase()){
                  groupId = ii;
                  groupInfo = dg;
                  groupPos = arrAnos.indexOf(vars.years[i]);
                  if(arrAnos.length < 2 && vars.years[i]=="2013"){
                    groupPos = 1;
                  }
                }
              });

              filtered = v.filter(function(d){
                return d.ano == vars.years[i];
              });
              sphere1List = v.filter(function(d){
                return d.ano == vars.years[i] &&
                d.esfera.toLowerCase() == "estadual" &&
                (d.decisao.toLowerCase().indexOf("acolhido") > -1 ||
                d.decisao.toLowerCase().indexOf("negado") > -1)
              });
              sphere2List = v.filter(function(d){
                return d.ano == vars.years[i] && d.esfera.toLowerCase() == "municipal" &&
                (d.decisao.toLowerCase().indexOf("acolhido") > -1 ||
                d.decisao.toLowerCase().indexOf("negado") > -1)
              });
              decision1List = v.filter(function(d){
                return d.ano == vars.years[i] &&
                d.esfera.toLowerCase() == "estadual" && d.decisao.toLowerCase().indexOf("acolhido") > -1;
              });
              decision2List = v.filter(function(d){
                return d.ano == vars.years[i] &&
                d.esfera.toLowerCase() == "estadual" && d.decisao.toLowerCase().indexOf("negado") > -1;
              });
              obj[index] = {
                ano: vars.years[i],
                total: filtered.length,
                esf1: sphere1List.length,
                esf2: sphere2List.length,
                acolhidos: decision1List.length,
                negados: decision2List.length,
                lista: filtered,
                group: groupId,
                g_data: groupInfo,
                g_position: groupPos
              };
            });
            return obj;
          })
          .entries(data);

          nested.forEach(function(d,i){
            susData[d.key] = d;
          });

          let nestedB = d3.nest()
            .key(function(d){ return d.uf.toLowerCase(); })
            .key(function(d){ return d.assunto.toLowerCase(); })
            .rollup(function(v){
              let obj = {}
              let decision1List, decision2List;
              decision1List = v.filter(function(d){
                return d.esfera.toLowerCase() == "estadual" && d.decisao.toLowerCase().indexOf("acolhido") > -1;
              });
              decision2List = v.filter(function(d){
                return d.esfera.toLowerCase() == "estadual" && d.decisao.toLowerCase().indexOf("negado") > -1;
              });

              obj = {
                acolhidos: decision1List.length,
                negados: decision2List.length,
                assunto: v[0].assunto,
                total: decision1List.length + decision2List.length,
                list: v
              }

              return obj;
            })
            .entries(data);

            // console.log(nested);
            susDataB = []
            nestedB.forEach(function(d,i){
              let list = [];
              d.values.forEach(function(dd,ii){
                if(dd.value.total > 0){
                  list.push(dd.value);
                }
              })

              list.sort(function(a,b){
                if (a.total > b.total) return -1;
                if (b.total > a.total) return 1;
                return 0;
              });

              susDataB[d.key] = list;
            });

          draw(true);
        })
    })

    maxSteps = d3.selectAll("#sus-intro .text-block p").size();

    d3.selectAll("#next-sus,#last-sus").on("click", function(d){
      let isNotLast = currStep < maxSteps;
      let increment = 1;
      let initStep = 1;

      if(d3.select(this).attr("id")=="last-sus"){
        isNotLast = currStep > 1;
        increment = -1;
        initStep = maxSteps;
      }

      if(isNotLast){
        currStep += increment;
      }else{
        //currStep = initStep;
      }

      draw(false);

      let stepper = d3.select("#sus-stepper");
      stepper.style("opacity", 0.7);
      stepper.selectAll("li").each(function(d, i){
        if(i!=currStep){
          d3.select(this).classed("active", false);
          d3.select("#sus-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", false);
          d3.select("#sus-intro").select(".text-block")
            .selectAll("ul").filter(function(p,j){ return j == i-1 }).classed("active", false);
        }else{
          d3.select(this).classed("active", true);
          d3.select("#sus-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", true);
          d3.select("#sus-intro").select(".text-block")
            .selectAll("ul").filter(function(p,j){ return j == i-1 }).classed("active", true);
        }
      })
    });
  }

  function showTooltip(d){
    let total = '';
    let access = ["acolhidos", "negados"];
    let accessName = ["suspensos", "mantidos"]
    let spheres = ["estadual", "municipal"];
    let type;
    let chart = d3.select(this).attr("class").split("-bar")[0];

    if(chart == "sus"){
      switch( currStep ){
          // Por ano, por estado
          case 1:
            type = d3.select(this).attr("class").split("bar")[1];
            total = "<b>Esfera "+spheres[type-1]+":</b> " +
              d["esf"+type] + " processos";
            break;

          // Acolhidos e negados
          case 2:
            type = d3.select(this).attr("class").split("bar")[1];
            total = d[access[type-1]] + ' processos';
            break;

          // Acolhidos e negados
          case 3:
            type = d3.select(this).attr("class").split("bar")[1];
            total = d[access[type-1]] + ' processos';
            break;

          default:
            break;
      }
    }else{
      type = d3.select(this).attr("class").split("bar")[1];
      total = d[access[type-1]] + " processos " + accessName[type-1];
    }
    let str = total;

    tooltip.classed("active", true);
    tooltip.html(str);

    positionTooltip();
  }

  function positionTooltip(){
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};

    let offset = {
        x: d3.select("#tooltip").style("width").slice(0,-2) / 2,
        y: d3.select("#tooltip").style("height").slice(0,-2) / 2
    }
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y;

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function draw(redraw){
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    if((redraw)){
      w = Number(svg.style("width").slice(0,-2));
      h = 10*45;
      /*
      h = window.innerHeight*0.7;
      if(h < 480){
        h = 480;
      }*/
      svgH = h - margin.top - margin.bottom;
      svgW = w - margin.left - margin.right;


      svg.attr("viewBox", "0 0 "+ w +" "+h);
    }


    switch( currStep ){
        // Por UF
        case 1:
            buildVis1(redraw);
            break;

        // Por UF, inacessíveis e em segredo
        case 2:
            //buildVis1(redraw);
            buildVis2(redraw);
            break;

        // Por UF, inacessíveis e em segredo
        case 3:
            //buildVis1(redraw);
            buildVis3(redraw);
            break;

        default:
            break;


    }

    // second chart
    let currData = susDataB[vars.states[currVars.state]];
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    if(redraw){
      wB = Number(svg2.style("width").slice(0,-2));
      hB = currData.length*45;//window.innerHeight*0.6;

      if(w < 480){
        marginB.left = 145;
        fontSize = 10;
      }else{
        marginB.left = 170;
        fontSize = 12;
      }

      svgHB = hB - marginB.top - marginB.bottom;
      svgWB = wB - marginB.left - marginB.right;

      svg2.attr("viewBox", "0 0 "+ wB +" "+hB);
    }

    buildVis4(redraw);

  }

  function changeSelections(target){
    let sel = selectors[0].node;
    let selStr = sel.options[sel.selectedIndex].value;
    let list = sel.parentNode.getAttribute("id").split("-")[2];
    let index = vars.states.indexOf(selStr.toLowerCase());

    currVars.state = index;
    draw(true);
  }

  function buildVis1(redraw){
    yearGroupData = ygData[vars.states[currVars.state]].values;
    let currData = susData[vars.states[currVars.state]].value;
    let total = d3.sum(currData, function(d){
      return d.esf1+d.esf2;
    })
    let duration = 500;
    if(redraw){ duration = 0; }

    //svgH = h - margin.top - margin.bottom;
    //svgW = w - margin.left - margin.right;

    yearGroupData.sort(function(a,b){
      if (a.anos > b.anos) return 1;
      if (b.anos > a.anos) return -1;
      return 0;
    })

    let maxX = d3.max(currData, function(d){
      return d.esf1 + d.esf2;
    });

    if(!vis1 || redraw){
      currVars.total = total;

      d3.select("#sus-val1").html(currVars.total);
      d3.select("#sus-val2").html(vars.stateTexts[currVars.state]);

      svg.select("#sus-vis1").remove();
      svg.selectAll(".axis").remove();
      vis1 = svg.append("g").attr("id", "sus-vis1");
      vis1.attr("transform", "translate(" + margin.left + "," + margin.top + ")")

      yScale = d3.scaleBand()
      .domain(currData.map(function(d){return d.ano;}))
      .range([0, svgH])
      .padding(0.25);

      yScale2 = d3.scaleBand()
      .domain(yearGroupData.map(function(d){return d.anos;}))
      .range([0, svgH])
      .padding(0.25);

      xScale = d3.scaleLinear()
      .domain([0,maxX])
      .range([0,svgW])
      .nice(4);

      xAxis = d3.axisBottom(xScale).ticks(4);
      yAxis = d3.axisLeft(yScale).tickPadding(10);
      if(w < 480){
        yAxis = d3.axisLeft(yScale).tickPadding(10);
      }

      gX = svg.append("g")
        .attr("class", "axis axis-x")
        .attr("transform", "translate(" + margin.left + "," + (h - margin.bottom) + ")")
        .call(xAxis);

      gY = svg.append("g")
        .attr("class", "axis axis-y")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .call(yAxis);

      gY.selectAll(".domain, line").remove();

      svg.append("text")
        .attr("class", "axis-title")
        .text("Número de processos")
        .attr("text-anchor","start")
        .attr("x", margin.left)
        .attr("y", svgH + margin.top + 35)

      vis1.selectAll(".sus-label1")
      .data(yearGroupData)
      .enter()
        .append("g")
        .attr("class","sus-label1")
        .attr("transform", function(d,i){
          let yVal = 0;
          let tYears = d.anos.split(",");
          let offset = (tYears.length == 1 && i == 0) ? -15 : 0;
          yVal = yScale(tYears[0]) - 6 + offset;

          return "translate(" + (-w - margin.left) + "," + yVal + ")";
        });

      vis1.selectAll(".sus-label1")
        .append("rect")
        .attr("class","sus-label1-bg")
        .attr("rx","4").attr("ry","4")
        .attr("fill","rgba(100,100,100,0.3)")
        .attr("width", w)
        .attr("y", 10)
        .attr("height", function(d){
          let years = d.anos.split(",");
          let num = years.length;

          return (yScale.bandwidth()*num) - ((svgH/60)) + 6;
          //return (yScale.bandwidth()*num) + ((svgH/30)*(num-1)) - 10;
        });

    /*  vis1.selectAll(".sus-label1")
        .append("text")
        .attr("class","sus-label1-txt")
        .style("font-size", "12px")
        .attr("fill", "#fff")
        .attr("transform","translate(0,6)")
        .attr("dy", 14)
        .attr("x", 12)
        .text(function(d){
          return d.gestao;
        })
        .call(textWrap, margin.left - 40);*/

      vis1.selectAll(".barGroup")
        .data(currData)
        .enter()
        .append("g")
        .attr("class","barGroup");

      vis1.selectAll(".barGroup")
        .append("rect")
        .attr("class","sus-bar2")
        .attr("rx","4").attr("ry","4")
        .attr("fill","#D33145")
        .attr("y", function(d){
          return yScale(d.ano) + (svgH/60);
        })
        .attr("x", 0)
        .attr("width", function(d){ return xScale(d.esf1 + d.esf2);})
        .attr("height", (yScale.bandwidth()-(svgH/30)) )
        .on("mouseover", showTooltip)
        .on("mousemove", positionTooltip)
        .on("mouseout", function(d){ tooltip.classed("active", false) });

      vis1.selectAll(".barGroup")
        .append("rect")
        .attr("class","sus-bar1")
        .attr("rx","4").attr("ry","4")
        .attr("fill","#F7D841")
        .attr("y", function(d){ return yScale(d.ano) + (svgH/60); })
        .attr("x", 0)
        .attr("width", function(d){ return xScale(d.esf1); })
        .attr("height", yScale.bandwidth()-(svgH/30) )
        .on("mouseover", showTooltip)
        .on("mousemove", positionTooltip)
        .on("mouseout", function(d){
          tooltip.classed("active", false)
        });

    }else{
      xScale.domain([0,maxX]).nice(4);

      vis1.selectAll(".sus-bar2")
        .transition().duration(duration)
        .attr("width", function(d){ return xScale(d.esf1 + d.esf2); })
        .attr("height", (yScale.bandwidth()-(svgH/30)) )
        .attr("y", function(d){
          return yScale(d.ano) + (svgH/60);
        });

      vis1.selectAll(".sus-bar1")
        .transition().duration(duration)
        .attr("width", function(d){ return xScale(d.esf1); })
        .attr("height", yScale.bandwidth()-(svgH/30) )

      vis1.selectAll(".sus-label1")
        .transition().duration(duration)
        .attr("transform", function(d,i){
          let yVal = 0;
          let tYears = d.anos.split(",");
          yVal = yScale(tYears[0])  - 6;

          return "translate(" + (-w - margin.left) + "," + yVal + ")";
      })

    }
    moveAxis(maxX, duration);

  }

  function buildVis2(redraw){
    let duration = 500;
    let currData = susData[vars.states[currVars.state]].value;
    let max1 = d3.max(currData, function(d){
      return d.acolhidos;
    });
    let max2 = d3.max(currData, function(d){
      return d.negados;
    });
    let maxX = d3.max(currData, function(d){
      return d.negados + d.acolhidos;
    });
    //console.log(maxX);

    if(redraw){
      buildVis1(redraw);
      duration = 0;
    }
    svgH = h - margin.top - margin.bottom;
    svgW = w - margin.left - margin.right;

    xScale.domain([0,maxX]).nice(4);
    moveAxis(maxX, duration);

    let totalDec1 = d3.sum(currData, function(d){
      return +d.acolhidos;
    });
    let totalDec2 = d3.sum(currData, function(d){
      return +d.negados;
    });

    d3.select("#sus-val3").html(totalDec1);
    d3.select("#sus-val4").html(totalDec2);

    vis1.selectAll(".sus-bar1")
      .transition().duration(duration)
      .attr("width", function(d){
        return xScale(d.acolhidos);
      })
      .attr("y", function(d){
        return yScale(d.ano) + (svgH/60);
      })
      .attr("height", yScale.bandwidth()-(svgH/30) );

    vis1.selectAll(".sus-bar2")
      .transition().duration(duration)
      .attr("width", function(d){
        return xScale(d.negados + d.acolhidos);
      })
      .attr("y", function(d, i){
        gY.selectAll(".tick text")
          .filter(function(dd,ii){ return ii == i; })
          .attr("y", function(dd){ return 0; })

        //return yScale(d.ano) + (svgH/60) + ((yScale.bandwidth()-(svgH/30))/2 + 2);
        return yScale(d.ano) + (svgH/60);
      })
      .attr("height", yScale.bandwidth()-(svgH/30) )

    vis1.selectAll(".sus-label1")
      .transition().duration(duration)
      .attr("transform", function(d,i){
        let yVal = 0;
        let tYears = d.anos.split(",");
        yVal = yScale(tYears[0])  - 6;

        return "translate(" + (-w - margin.left) + "," + yVal + ")";
      })

  }

  function buildVis3(redraw){
    let duration = 500;
    let currData = susData[vars.states[currVars.state]].value;
    let max1 = d3.max(currData, function(d){
      return d.acolhidos;
    });
    let max2 = d3.max(currData, function(d){
      return d.negados;
    });
    let maxX = d3.max(currData, function(d){
      return d.negados + d.acolhidos;
    });

    if(redraw){
      buildVis2(redraw);
      duration = 0;
    }

    svgH = h - margin.top - margin.bottom;
    svgW = w - margin.left - margin.right;

    xScale.domain([0,maxX]).nice(4);
    moveAxis(maxX, duration);

    if(w < 480){
      vis1.selectAll(".axis-y text")
      .style("opacity",0)
    }else{
      vis1.selectAll(".axis-y text")
      .style("opacity",1)
    }

    vis1.selectAll(".sus-label1")
      .transition().duration(duration)
      .attr("transform", function(d,i){
        let yVal = 0;
        let tYears = d.anos.split(",");
        let offset = (tYears.length == 1 && i == 0) ? -15 : 0;
        yVal = yScale(tYears[0]) + 2 + offset;

        return "translate("+ (-margin.left) +"," + yVal + ")";
      })

    vis1.selectAll(".sus-bar1")
      .transition().duration(duration)
      .attr("width", function(d){
        return xScale(d.acolhidos);
      })
      /*.attr("y", function(d){
        return yScale(d.ano) + (svgH/60);
      })*/
      .attr("y", function(d,i){
        let pos = d.g_position;
        return yScale(d.ano) + (svgH/60) + ((1-pos)*15);
      })
      .attr("height", yScale.bandwidth()-(svgH/30) );
      /*
      .attr("y", function(d,i){
        let pos = d.g_position;
        return yScale(d.ano) + (svgH/60) + ((1-pos)*15);
      })
      .attr("height", (yScale.bandwidth()-(svgH/30))/2 - 2 );
      */

    vis1.selectAll(".sus-bar2")
      .transition().duration(duration)
      .attr("width", function(d){
        return xScale(d.negados + d.acolhidos);
      })
      .attr("y", function(d, i){
        let pos = d.g_position;
        gY.selectAll(".tick text")
          .filter(function(dd,ii){ return ii == i; })
          .attr("y", function(dd){ return 0; })

        //return yScale(d.ano) + (svgH/60) + ((yScale.bandwidth()-(svgH/30))/2 + 2);
        //return yScale(d.ano) + (svgH/60);
        return yScale(d.ano) + (svgH/60) + ((1-pos)*15);
      })
      .attr("height", yScale.bandwidth()-(svgH/30) )
      /*
      .attr("width", function(d){
        return xScale(d.negados);
      })
      .attr("y", function(d,i){
        let pos = d.g_position;
        gY.selectAll(".tick text")
          .filter(function(dd,ii){ return ii == i; })
          .attr("y", function(dd){ return ((1-pos)*15); })

        return yScale(d.ano) + (svgH/60) + ((yScale.bandwidth()-(svgH/30))/2 + 2) + ((1-pos)*15);
      })
      .attr("height", (yScale.bandwidth()-(svgH/30))/2 - 2 )
      */

    vis1.selectAll(".sus-label1 text").remove();

    vis1.selectAll(".sus-label1")
      .append("text")
      .attr("class","sus-label1-txt")
      .style("font-size", "12px")
      .attr("fill", "#fff")
      .attr("transform","translate(0,6)")
      .attr("dy", -1)
      .attr("x", 0)
      .text(function(d){
        return d.gestao;
      })
      //.call(textWrap, w - 20);

  }

  function buildVis4(redraw){
    let currData = susDataB[vars.states[currVars.state]];
    let total = d3.sum(currData, function(d){
      return +d.total;
    })

    let duration = 500;
    if(redraw){ duration = 0; }

    let maxX = d3.max(currData, function(d){
      return d.total;
    });

    svg2.selectAll(".axis").remove();
    svg2.selectAll("#sus2-vis").remove();
    vis2 = svg2.append("g").attr("id", "sus2-vis");
    //vis.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    yScaleB = d3.scaleBand()
    .domain(currData.map(function(d){ return d.assunto; }))
    .range([0, svgHB])
    .padding(0.1);

    xScaleB = d3.scaleLinear()
    .domain([0, maxX])
    .range([0, svgWB])
    .nice(4);

    xAxisB = d3.axisBottom(xScaleB).ticks(4);
    yAxisB = d3.axisLeft(yScaleB).tickPadding(10);

    gXB = svg2.append("g")
      .attr("class", "axis axis-x")
      .attr("transform", "translate(" + marginB.left + "," + (hB - marginB.bottom) + ")")
      .call(xAxisB);

    gYB = svg2.append("g")
      .attr("class", "axis axis-y")
      .attr("transform", "translate(" + marginB.left + "," + marginB.top + ")")
      .call(yAxisB);

    gYB.selectAll(".tick text")
      .attr("font-size", fontSize+"px")
      .attr("text-anchor", "end")
      .attr("x", -10)//margin.left)
      .call(textWrap, marginB.left - 10);

    gYB.selectAll(".domain, line").remove();

    vis2.append("text")
      .attr("class", "axis-title")
      .text("Número de processos")
      .attr("text-anchor","start")
      .attr("x", marginB.left)
      .attr("y", svgHB + marginB.top + 35)

    let group1 = vis2.append("g").attr("id", "sus2-g1");

    let bars = group1.selectAll(".sus2-left")
    .data(currData)
    .enter()
    .append("g")
    .attr("class", "sus2-left")
    .attr("transform","translate("+marginB.left+"," + marginB.top + ")");

    bars.append("rect")
    .attr("class", "sus2-bar2")
    .attr("height", yScaleB.bandwidth()-4)
    .attr("fill", "#D33145")
    .attr("rx", "3").attr("ry", "3")
    .attr("width", function(d){
      return xScaleB(d.total);
    })
    .attr("y",function(d,i){
      return yScaleB(d.assunto);
    })
    .on("mouseover", showTooltip)
    .on("mousemove", positionTooltip)
    .on("mouseout", function(d){ tooltip.classed("active", false) });

    bars.append("rect")
    .attr("class", "sus2-bar1")
    .attr("height", yScaleB.bandwidth()-4)
    .attr("fill", "#F7D841")
    .attr("rx", "3").attr("ry", "3")
    .attr("width", function(d){
      return xScaleB(d.acolhidos);
    })
    .attr("y",function(d,i){
      return yScaleB(d.assunto);
    })
    .on("mouseover", showTooltip)
    .on("mousemove", positionTooltip)
    .on("mouseout", function(d){ tooltip.classed("active", false) });

  }

  function moveAxis(maxX, duration){
    vis1.transition().duration(duration)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    xScale.domain([0,maxX]).range([0,svgW]).nice(4);
    //yScale.range([0, svgH]);

    vis1.select("axis-x")
    .transition().duration(duration)
    .call(xAxis)

    gX.transition().duration(duration)
      .attr("transform", "translate(" + margin.left + "," + (h - margin.bottom) + ")")
      .call(xAxis.scale(xScale));

    gY.transition().duration(duration)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    gY.selectAll(".domain, line").remove();
  }

  function textWrap(text, width) {
    text.each(function() {
      var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = fontSize + 3, // ems
          y = text.attr("y") - 5,
          dy = parseFloat(text.attr("dy")),
          dx = parseFloat(text.attr("x")),
          tspan = text.text(null).append("tspan").attr("x", dx).attr("y", y).attr("dy", dy + "px");
      while (word = words.pop()) {
        line.push(word);
        tspan.text(line.join(" "));
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
          tspan = text.append("tspan").attr("text-anchor", "end").attr("x", dx).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "px").text(word);
        }
      }
    });
  }


  window.addEventListener("resize", function(){
    draw(true);
    //animateChange();
  });
  let initTimeout = setTimeout(setup, 100);

  return (
    <section>
      <div id="sus" className="chart-block">
        <h3>Suspensão de segurança
          <div id="sus-selects" className="selector-group">
              <div id="select-states-sus" className="select">
              <select>
                  <option value="CE">CE</option>
                  <option value="PR">PR</option>
                  <option value="SP" defaultValue>SP</option>
              </select>
              </div>
          </div>
        </h3>
        <div id="sus-intro" className="intro-block">
          <p>Suspensão de segurança é um dispositivo que confere ao presidente do Tribunal a competência para suspender os efeitos de decisões tomadas contra o poder público por juízes de primeira instância</p>

          <ul id="sus-stepper" className="dot-stepper">
            <li><a id="last-sus" className="arrow-last"></a></li>
            <li className="active"><a className="dot"></a></li>
            <li><a className="dot"></a></li>
            <li><a className="dot"></a></li>
            <li><a id="next-sus" className="arrow-next pulse"></a></li>
          </ul>
          <div className="text-block">
            <p className="active">De 2013 a 2018, houve um total de <b id="sus-val1">0</b> processos de suspensão de segurança das esferas estadual e municipal</p>
            <p>O interesse do justa está na esfera estadual. Todos os dados a seguir referem-se, portanto, a esse recorte da base de dados</p>
            <p>As decisões sobre os processos foram tomadas por diferentes gestões do TJ. Os mandatos nas presidências dos Tribunais de justiça têm duração de 2 anos</p>

            <ul id="sus-leg1" className="legenda active">
              <span className="title-desc block">Total por ano, por esfera</span>
              <li><span className="sq ga"></span> Esfera estadual</li>
              <li><span className="sq gn"></span> Esfera municipal</li>
            </ul>
            <ul id="sus-leg2" className="legenda">
              <span className="title-desc block">Processos da esfera estadual mantidos e suspensos</span>
              <li><span className="sq ga"></span> Suspensos</li>
              <li><span className="sq gn"></span> Mantidos</li>
            </ul>
            <ul id="sus-leg3" className="legenda">
              <span className="title-desc block">Processos da esfera estadual mantidos e suspensos, por gestão do tribunal</span>
              <li><span className="sq ga"></span> Suspensos</li>
              <li><span className="sq gn"></span> Mantidos</li>
            </ul>
          </div>
        </div>

        <svg id="svg-sus-1" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+w+" "+h+""}>
        </svg>

        <p className="chart-source">
          <b>Fonte:</b> Ceará - TJCE / Paraná - TJPR / São Paulo - TJSP &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b>  JUSTA
        </p>
      </div>

      <div id="sus2" className="chart-block">
        <ul id="sus2-leg2" className="legenda active">
          <span className="title-desc block">Decisões mantidas e suspensas, por assunto, na esfera estadual</span>
          <li><span className="sq ga"></span> Suspensas</li>
          <li><span className="sq gn"></span> Mantidas</li>
        </ul>
        <svg id="svg-sus-2" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+w+" "+h+""}>
        </svg>
        <p className="chart-source">
          <b>Fonte:</b> Ceará - TJCE / Paraná - TJPR / São Paulo - TJSP &nbsp;&nbsp;|&nbsp;&nbsp;<b>Elaboração:</b>  JUSTA
        </p>
      </div>
    </section>
  )
}

export default Suspension;
