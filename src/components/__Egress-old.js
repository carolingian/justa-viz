import * as d3 from "d3";
import React from 'react'
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function Egress(){
  let w = window.innerWidth - 20,
      h = window.innerHeight*0.5;
  let svg;
  let egData;

  let canvas,
      context,
      simulation,
      tick = 0;
  let step = 1;
  let maxSteps = 4;
  let egRatio = 0.45;
  let nIncrement = 0.05;

  // settings
  let nodes = [];
  let strength = -0.2;         // default repulsion
  let centeringStrength = 0.01; // power of centering force for two clusters
  let velocityDecay = 0.1;     // velocity decay: higher value, less overshooting
  let outerRadius = 200;        // new nodes within this radius
  let innerRadius = 100;
  let diameter;

  // let nodes = [];
  // let strength = -0.1;         // default repulsion
  // let centeringStrength = 0.005; // power of centering force for two clusters
  // let velocityDecay = 0.05;     // velocity decay: higher value, less overshooting
  // let outerRadius = 250;        // new nodes within this radius
  // let innerRadius = 100;
  // new nodes outside this radius, initial nodes within.
  let vizCenters = [
    [[3*(w/6),h/2],[3*(w/6),h/2]],  // step 0
    [[2*(w/6),h/2],[4*(w/6),h/2]],  // step 1
    [[2*(w/6),h/2],[4*(w/6),h/2]],  // step 2
    [[2*(w/6),h/2],[4*(w/6),h/2]]   // step 3
  ];
  let startCenter = vizCenters[0][0];  // new nodes/initial nodes center point
  let endCenter = vizCenters[0][1];  // destination center
  let n = 600;		              // number of initial nodes
  let cycles = 2000;	            // number of ticks before stopping
  let migratedTotal = 0;

  function setup(){
    canvas = d3.select("#canvas-eg1");
    context = canvas.node().getContext('2d');

    for(var i = 0; i < n; i++) {
    	nodes.push(random());
    }

    draw();

    d3.selectAll("#next-eg,#last-eg").on("click", function(d){
      let isNotLast = step < maxSteps;
      let increment = 1;
      let initPart = 1;

      if(d3.select(this).attr("id")=="last-eg"){
        isNotLast = step > 1;
        increment = -1;
        initPart = maxSteps;
      }

      if(isNotLast){
        step += increment;
      }else{
        step = initPart;
      }

      tick = 0;
      simulation.restart();
      simulation.nodes(nodes);

      updateStepper();
    });

    // load national egress data
    /*d3.csv("data/gender-race-uf.csv", function(d,i,cols){
      return d;
    })
    .then(function(data) {
      egData = data;
      //draw();
    })
    .catch(function(error){
       console.log("error on 'egress'");
    });*/
  }

  function draw(){
    w = window.innerWidth - 20;
    h = window.innerHeight*0.5;
    if(h < 400){
      h = 400;
    }
    canvas.attr("width", w);
    canvas.attr("height", h);

    startCenter = vizCenters[step-1][0];
    endCenter = vizCenters[step-1][1];

    simulation = d3.forceSimulation()
      .force("charge", d3.forceManyBody().strength(function(d) { return d.strength; } ))
    	.force("x1",d3.forceX().x(function(d) { return d.migrated ? vizCenters[step-1][1][0] : vizCenters[step-1][0][0] }).strength(centeringStrength))
    	.force("y1",d3.forceY().y(function(d) { return d.migrated ? vizCenters[step-1][1][1] : vizCenters[step-1][0][1] }).strength(centeringStrength))
    	.tick(5)
      .alphaDecay(0)
    	.velocityDecay(velocityDecay)
        .nodes(nodes)
        .on("tick", ticked);
    //simulation.tick(100);
  }

  function random(){
  	var angle = Math.random() * Math.PI * 2;
  	var distance = Math.random() * (outerRadius - innerRadius) + innerRadius;
  	var x = Math.cos(angle) * distance + vizCenters[step-1][0][0];
  	var y = Math.sin(angle) * distance + vizCenters[step-1][0][1];

  	return {
  	   x: x,
  	   y: y,
  	   strength: strength,
  	   migrated: false,
       reincident: false
  	   }
  }

  function ticked() {
    tick++;

    if(step == 1){
      this.nodes(nodes);
      maxY = d3.max(nodes, function(d) { return +d.y;} );
      minY = d3.min(nodes, function(d) { return +d.y;} );
      diameter = maxY - minY;

      if(tick >= cycles){
        this.stop();
      }
      migratedTotal = 0;
    }else if(step == 2){
      //nodes.push(random()); // create a node
      if(migratedTotal >= Math.round(n*0.45)){
        tick = 0;
      }

      if(migratedTotal < Math.round(n*egRatio)){
        var maxY = 0;
        var minY = 0;
        var migrating;

        for(var i=0; i<2; i++){
          migrating = this.find((Math.random() - 0.5) * 150 + vizCenters[step-1][0][0], (Math.random() - 0.5) * 150 + vizCenters[step-1][0][1], 10);
          if(migrating){
            migrating.migrated = true;
            this.nodes(nodes);
            migratedTotal++;
          }
        }
      }
    }

    if(step < 2){
      migratedTotal = 0;
    }
    if(tick >= cycles){
      this.stop();
      tick = 0;
    }

  	context.clearRect(0,0,w,h);

  	nodes.forEach(function(d) {
      if(step < 2){
        d.migrated = false;
      }
  		context.beginPath();
  		context.fillStyle = d.migrated ? "#fff200" : "#00E1FF";
  		context.arc(d.x,d.y,2,0,Math.PI*2);
  		context.fill();
  	});

    var innerDiameter = (diameter*100) / 169;
    //Math.sqrt((Math.PI*Math.pow(diameter/2,2)*100)/(169*Math.PI))*2;

    if(step == 1 && tick >= 100){
      context.beginPath();
      context.strokeStyle = "#FFFFFF";
      context.lineWidth = 3;
      context.setLineDash([10, 6]);
      context.arc(vizCenters[step-1][0][0], vizCenters[step-1][0][1], innerDiameter/2, 0, Math.PI*2);
      context.stroke();

      // linha de legenda do circulo central
      /*
      context.beginPath();
      context.strokeStyle = "#FFFFFF";
      context.lineWidth = 3;
      context.setLineDash([1, 0]);
      context.moveTo(vizCenters[step-1][0][0]+(innerDiameter/2), vizCenters[step-1][0][1]);
      context.lineTo(vizCenters[step-1][0][0]+(innerDiameter/2)+50, vizCenters[step-1][0][1]);
      context.stroke();

      context.beginPath();
      context.strokeStyle = "#00E1FF";
      context.lineWidth = 3;
      context.setLineDash([1, 0]);
      context.moveTo(vizCenters[step-1][0][0]-(diameter/2), vizCenters[step-1][0][1]);
      context.lineTo(vizCenters[step-1][0][0]-(diameter/2)-15, vizCenters[step-1][0][1]);
      context.stroke();*/
    }
  }

/*

  Math.PI*Math.pow(d/2,2) -- 169
  Math.PI*Math.pow(x/2,2) -- 100

  Math.pow(x/2,2) = (Math.PI*Math.pow(d/2,2)*100)/(169*Math.PI)

  x = Math.sqrt((Math.PI*Math.pow(d/2,2)*100)/(169*Math.PI))*2

*/

  function updateStepper(){
    let stepper = d3.select("#egress-stepper");
    stepper.style("opacity", 0.7);

    stepper.selectAll("li").each(function(d, i){
      if(i!=step){
        d3.select(this).classed("active", false);
        d3.select("#egress-intro").select(".text-block")
          .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", false);
      }else{
        d3.select(this).classed("active", true);
        d3.select("#egress-intro").select(".text-block")
          .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", true);
      }
    })
  }

  window.addEventListener("resize", function(){
    //draw();
  });
  let initTimeout = setTimeout(setup, 100);

  return (
    <section id="egress">
    <h3>Egressos do Sistema Prisional</h3>
    <div id="egress-intro" className="intro-block">
      <ul id="egress-stepper" className="dot-stepper">
        <li><a id="last-eg" className="arrow-last"></a></li>
        <li className="active"><a className="dot"></a></li>
        <li><a className="dot"></a></li>
        <li><a className="dot"></a></li>
        <li><a className="dot"></a></li>
        <li><a id="next-eg" className="arrow-next pulse"></a></li>
      </ul>
      <div className="text-block">
        <p className="active">As penitenciárias de SP operam com <b>superlotação</b>, com muito mais presos que a sua capacidade</p>
        <p>A cada ano, uma parte dos presos cumpre sua pena e retorna à sociedade</p>
        <p>Mas sem reintegração ou recolocação profissional, um em cada 4 <b>egressos</b> retorna à prisão 5 anos após sua soltura</p>
        <p>E a desigualdade é ainda maior na evolução da carreira para o cargo de <b>desembargador</b></p>
      </div>
    </div>
    <div className="">
      <canvas id="canvas-eg1" width={w} height={h}></canvas>
    </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    </section>
  )
}

export default Egress;
