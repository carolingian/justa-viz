import React from 'react';

function Select(props){
  let i, j, selElmnt, a, b, c;
  let element = props.target;
  let callback = props.callback;

  this.node = element.querySelector("select");

  selElmnt = element.getElementsByTagName("select")[0];
  /* For each element, a new div will act as the selected item: */
  if(!a){
    a = document.createElement("DIV");
    a.setAttribute("class", "selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    element.appendChild(a);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");

    for (j = 0; j < selElmnt.length; j++) {
      /* For each option in the original select element,
      create a new DIV that will act as an option item: */
      c = document.createElement("DIV");
      c.innerHTML = selElmnt.options[j].innerHTML;
      c.addEventListener("click", function(e) {
          /* When an item is clicked, update the original select box,
          and the selected item: */
          let y, i, k, s, h;
          s = this.parentNode.parentNode.getElementsByTagName("select")[0];
          h = this.parentNode.previousSibling;

          for (i = 0; i < s.length; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
              s.selectedIndex = i;
              h.innerHTML = this.innerHTML;
              y = this.parentNode.getElementsByClassName("same-as-selected");
              for (k = 0; k < y.length; k++) {
                y[k].removeAttribute("class");
              }
              this.setAttribute("class", "same-as-selected");
              break;
            }
          }
          callback();
          h.click();
      });
      b.appendChild(c);
    }
    element.appendChild(b);
    a.addEventListener("click", function(e) {
      /* close any other select boxes,
      and open/close the current one: */
      e.stopPropagation();
      closeSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("arrow-active");
    });
  }

  function closeSelect(el) {
    /* close all select boxes in the document,
    except the current one */
    let x, y, i, arrNo = [];
    x = document.querySelectorAll(".select-items");
    y = document.querySelectorAll(".selected");

    for (i = 0; i < y.length; i++) {
      if (el == y[i]) {
        arrNo.push(i)
      } else {
        y[i].classList.remove("arrow-active");
      }
    }
    for (i = 0; i < x.length; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
      }
    }
  }

  /* If the user clicks anywhere outside the select box,
  then close all select boxes: */
  document.addEventListener("click", closeSelect);

  return;
}
export default Select;
