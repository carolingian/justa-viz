import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import './styles/Reset.css';
import './styles/Main.scss';

import App from './components/App';

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
